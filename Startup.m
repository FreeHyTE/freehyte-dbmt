function Startup

%%
% Splash screen sequence
timer = tic;
ShowSplashForSecs = 1;

s = SplashScreen( 'Splashscreen', 'Splash.png', ...
    'ProgressBar', 'on', ...
    'ProgressPosition', 5, ...
    'ProgressRatio', 0.4 );
s.addText( 358, 320, 'direct Boundary Method Toolbox', 'FontSize', 30, 'Color', [0 0 0.6] );
s.addText( 540, 360, 'v1.3', 'FontSize', 25, 'Color', [0.2 0.2 0.5] );
s.addText( 190, 190, 'Loading...', 'FontSize', 20, 'Color', 'white' );

ElapsedTime = toc(timer);
if ElapsedTime < ShowSplashForSecs
    pause(ShowSplashForSecs - ElapsedTime);
end
delete(s);

fclose('all');

dBMT_StructDef(2);