function PlotFieldsOriginatedByLayerPotentials(xmesh,ymesh,U,dU_dx,dU_dy)

% PLOTFIELDSORIGINATEDBYLAYERPOTENTIALS plots the solution for the given
% mesh of points

% PlotFieldsOriginatedByLayerPotentials is called by MAIN***. 
% Input data: 
% xmesh,ymesh   - mesh of points 
% U,dU_dx,dU_dy - solution: potentials and its spatial derivatives in 
%                     (xmesh,ymesh)

% Output/returns to MAIN***:
% no output


Fig = figure;
set(Fig,'name','Refinement Orders, Temperature and Flux Fields',...
    'numbertitle','off','color','w');

%% Potential plot
% Preparing the mesh for the potential plot
Temp = subplot(2,2,2);
hold on; title('Potential Field','FontWeight','bold');
axis([min(min(xmesh)) max(max(xmesh)) min(min(ymesh)) max(max(ymesh))]);

surfScatteredData(xmesh, ymesh, real(U)) ;
colormap jet;
caxis(caxis); colorbar; axis off;    
daspect([1 1 1])

%% Flux plot, in X
% Preparing the mesh for the Qx plot
Qx = subplot(2,2,3);
hold on; title('Flux field, x direction','FontWeight','bold');
axis([min(min(xmesh)) max(max(xmesh)) min(min(ymesh)) max(max(ymesh))]);

surfScatteredData(xmesh, ymesh, real(dU_dx)) ;
colormap jet;
caxis(caxis); colorbar; axis off;    
daspect([1 1 1])

%% Flux plot, in Y
% Preparing the mesh for the Qy plot
Qy = subplot(2,2,4);
hold on; title('Flux field, y direction','FontWeight','bold');
axis([min(min(xmesh)) max(max(xmesh)) min(min(ymesh)) max(max(ymesh))]);

surfScatteredData(xmesh, ymesh, real(dU_dy)) ;
colormap jet;
caxis(caxis); colorbar; axis off;
daspect([1 1 1])


end % function PlotFieldsOriginatedByLayerPotentials(xmesh,ymesh,U,dU_dx,dU_dy)


