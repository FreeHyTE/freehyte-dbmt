function varargout = dBMT_StructDef(varargin)
% DBMT_STRUCTDEF MATLAB code for dBMT_StructDef.fig
%      DBMT_STRUCTDEF, by itself, creates a new DBMT_STRUCTDEF or raises the existing
%      singleton*.
%
%      H = DBMT_STRUCTDEF returns the handle to a new DBMT_STRUCTDEF or the handle to
%      the existing singleton*.
%
%      DBMT_STRUCTDEF('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in DBMT_STRUCTDEF.M with the given input arguments.
%
%      DBMT_STRUCTDEF('Property','Value',...) creates a new DBMT_STRUCTDEF or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before dBMT_StructDef_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to dBMT_StructDef_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help dBMT_StructDef

% Last Modified by GUIDE v2.5 07-Jan-2021 15:18:30

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @dBMT_StructDef_OpeningFcn, ...
                   'gui_OutputFcn',  @dBMT_StructDef_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% UIWAIT makes dBMT_StructDef wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = dBMT_StructDef_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


%% OPENING FUNCTION
% * Executes just before dBMT_StructDef is made visible;
% * Reads the previous data in the local |mat| file and fills in the
% fields.
function dBMT_StructDef_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to dBMT_StructDef (see VARARGIN)

%%
% If no varargin was defined, it is set to zero and saved to handles
if isempty(varargin)
    handles.varargin = 0;
else
    handles.varargin = varargin{1};
end

%%
% Setting warnings to off. These warnings are caused by missing files and
% variables before the local |mat| files are written for the first time and
% by the possibility that the problem is purely Neumann or Dirichlet. The
% warnings are re-activated after the successful execution, at the end of
% the |main.m| function.
warning('off','MATLAB:DELETE:FileNotFound');
warning('off','MATLAB:load:variableNotFound');


%%
% Creates the |handles| structure
% Choose default command line output for dBMT_StructDef
handles.output = hObject;


%% 
% Getting the current working folder (in R2012 may be different from the
% folder you started the app from!)
handles.WorkingFolder = pwd;

%% 
% Preparing a structure that stores all the necessary UserInput data.
UI.L = [] ;
UI.B = [] ;
UI.Nx = [] ;
UI.Ny = [] ;
UI.EdgesOrder = [] ;
UI.NumberGaussPoints = [] ;
UI.k = [] ;
UI.offset = [] ;
UI.MeshOption = [] ;
UI.SolMethod = [] ;
UI.EquationType = [] ;
UI.lambda = [] ;
UI.ProblemType = [] ;
UI.incident.amplitude = [] ; % ext. Helmholtz incident plane wave amplitude
UI.incident.angle = [] ; % ext. Helmholtz incident plane wave angle



%%
% If there exists a local |mat| file, it loads its key elements to fill in
% the fields with data taken from the previous run.
if exist('./dBMT_StructDef.mat','file')

    load('dBMT_StructDef', 'UI') ;
    
    %%
    % In rare situations, no values are stored for L, B, Nx, Ny. If this is
    % the case, it reads NaN and thus ChangesQ always results 1. To avoid
    % this, when NaN is read, it is substituted by zero.
    if isnan(UI.L)
        UI.L=0; UI.B=0; UI.Nx=0; UI.Ny=0;
    end
 
    % loading the values to the interface
    set(handles.edit_DimX,'String',sprintf('%d',UI.L));
    set(handles.edit_DimY,'String',sprintf('%d',UI.B));
    set(handles.edit_NLoopX,'String',sprintf('%d',UI.Nx));
    set(handles.edit_NLoopY,'String',sprintf('%d',UI.Ny));
    set(handles.edit_OrderEdge,'String',sprintf('%d',UI.EdgesOrder));
    set(handles.edit_NGP,'String',sprintf('%d',UI.NumberGaussPoints));
    set(handles.edit_offset,'String',sprintf('%g',UI.offset));
    set(handles.edit_lambda,'String',sprintf('%g',UI.lambda));
    set(handles.popupmenu_mesh,'Value',UI.MeshOption);
    set(handles.popupmenu_SolMethod,'Value',UI.SolMethod);
    set(handles.popupmenu_EquationType,'Value',UI.EquationType);
    set(handles.popupmenu_ProblemType,'Value',UI.ProblemType);

    if isnan(UI.k)
        set(handles.edit_k,'String',sprintf(''));
    else        
        if imag(UI.k) == 0
            set(handles.edit_k,'String',sprintf('%g',UI.k));
        elseif real(UI.k) == 0
            set(handles.edit_k,'String',sprintf('%gi',imag(UI.k) ));
        else
            set(handles.edit_k,'String',sprintf('%g+%gi',real(UI.k), imag(UI.k) ));
        end
    end
    
    % input fields for incident wave parameters 
    if isempty(UI.incident.amplitude) || isnan(UI.incident.amplitude) % if incident_amplitude is not set, it is saved  ...
        UI.incident.amplitude = [] ; % as NaN. To avoid showing 'NaN' in edit field, set it to []. 
    end
    set(handles.edit_incident_amplitude,'String',sprintf('%g',UI.incident.amplitude));
    
    if isnan(UI.incident.angle) % if incident_angle is not set, it is saved  ...
        UI.incident.angle = [] ; % as NaN. To avoid showing 'NaN' in edit field, set it to []. 
    end
    set(handles.edit_incident_angle,'String',sprintf('%g',UI.incident.angle));
    
       
    %%
    % If |MeshOption = 2|, that is, the mesh generation is automatic, it
    % makes no sense to edit the fields associated to the regular mesh
    % generator. They become inactive.
    if UI.MeshOption == 1
        set(handles.edit_DimX, 'enable', 'on');
        set(handles.edit_DimY, 'enable', 'on');
        set(handles.edit_NLoopX, 'enable', 'on');
        set(handles.edit_NLoopY, 'enable', 'on');
    else
        set(handles.edit_DimX, 'enable', 'off');
        set(handles.edit_DimY, 'enable', 'off');
        set(handles.edit_NLoopX, 'enable', 'off');
        set(handles.edit_NLoopY, 'enable', 'off');
    end
    
    %%
    % If |SolMethod = 2|, that is, the MFS solver is chosen, the lambda
    % field becomes visible
    if UI.SolMethod == 2
        set(handles.edit_lambda, 'visible', 'on');
        set(handles.text_lambda, 'visible', 'on');
    else
        set(handles.edit_lambda, 'visible', 'off');
        set(handles.text_lambda, 'visible', 'off');
    end

    %% 
    % if EquationType = 2, that is, the problem to solve is the Helmholtz
    % equation, the k field becomes visible and the correct equation
    % decription is saved
    if UI.EquationType == 2 
        set(handles.edit_k, 'visible', 'on');
        set(handles.text_k, 'visible', 'on');
        mystr = '$$ \nabla^2 u \left( x,y \right) + k^2 \cdot u \left( x,y \right) = 0 $$';
        
        if UI.ProblemType == 2 % if exterior problem 
            
            set(handles.text_ext_Helmholtz_params_caption, 'visible', 'on');               
            set(handles.edit_incident_amplitude, 'visible', 'on');
            set(handles.text_incident_amplitude, 'visible', 'on');   
            set(handles.edit_incident_angle, 'visible', 'on');
            set(handles.text_incident_angle, 'visible', 'on');   
            
        else
            set(handles.text_ext_Helmholtz_params_caption, 'visible', 'off');  
            set(handles.edit_incident_amplitude, 'visible', 'off');
            set(handles.text_incident_amplitude, 'visible', 'off');              
            set(handles.edit_incident_angle, 'visible', 'off');
            set(handles.text_incident_angle, 'visible', 'off');

        end
        
        
        
        
    % if EquationType = 1, that is, the problem to solve is Laplace
    % equation, the k and incidentWaveAngle fields become invisible 
    % and the correct equation gdecription is saved
    else
        set(handles.edit_k, 'visible', 'off');
        set(handles.text_k, 'visible', 'off');
        
        set(handles.text_ext_Helmholtz_params_caption, 'visible', 'off');  
        set(handles.edit_incident_amplitude, 'visible', 'off');
        set(handles.text_incident_amplitude, 'visible', 'off');   
        set(handles.edit_incident_angle, 'visible', 'off');
        set(handles.text_incident_angle, 'visible', 'off');        
        
        mystr = '$$\nabla^2 u \left( x,y \right) = 0 $$';
    end   
        
    % writing the current equation type on the interface
    try
        A1 = annotation(handles.uipanel2,'textbox', [0.02,0.71,0.1,0.1],'string',mystr,...
            'interpreter','latex', 'FontSize',14,'LineStyle','none',...
            'FontWeight','bold');
    catch errors
        A1 = annotation(gcf,'textbox', [0.02,0.71,0.1,0.1],'string',mystr,...
            'interpreter','latex', 'FontSize',14,'LineStyle','none',...
            'FontWeight','bold');
    end
    
else   % if no file to load exists, it assumes a Laplace problem
    set(handles.edit_k, 'visible', 'off');
    set(handles.text_k, 'visible', 'off');
    
    % LaTeX description of the problem
    mystr = '$$\nabla^2 u \left( x,y \right) = 0 $$';
    
    % writing the current equation type on the interface
    try
        A1 = annotation(handles.uipanel2,'textbox', [0.02,0.71,0.1,0.1],'string',mystr,...
            'interpreter','latex', 'FontSize',14,'LineStyle','none',...
            'FontWeight','bold');
    catch errors
        A1 = annotation(gcf,'textbox', [0.02,0.71,0.1,0.1],'string',mystr,...
            'interpreter','latex', 'FontSize',14,'LineStyle','none',...
            'FontWeight','bold');
    end
end

% storing the A1 annotation in handles to make it accessible
handles.A1 = A1;

%% 
% LaTeX description of the boundary conditions
mystr2 = '$$ u \left( s \right) = \overline{u} \left( s \right) $$';
mystr3 = '$$ \frac{\partial u \left( s \right)}{\partial n} = \overline{q} \left( s \right) $$';
mystr4 = '$$ \frac{\partial u \left( s \right)}{\partial n} = \overline{g} \left( s \right) + r \cdot u \left( s \right) $$';

% writing the current equation type on the interface
try
    annotation(handles.uipanel2,'textbox', [0.02,0.62,0.1,0.1],'string',mystr2,...
        'interpreter','latex', 'FontSize',14,'LineStyle','none',...
        'FontWeight','bold');
    annotation(handles.uipanel2,'textbox', [0.02,0.56,0.1,0.1],'string',mystr3,...
        'interpreter','latex', 'FontSize',14,'LineStyle','none',...
        'FontWeight','bold');
    annotation(handles.uipanel2,'textbox', [0.02,0.47,0.1,0.1],'string',mystr4,...
        'interpreter','latex', 'FontSize',14,'LineStyle','none',...
        'FontWeight','bold');
catch errors
    annotation(gcf,'textbox', [0.02,0.62,0.1,0.1],'string',mystr2,...
        'interpreter','latex', 'FontSize',14,'LineStyle','none',...
        'FontWeight','bold');
    annotation(gcf,'textbox', [0.02,0.56,0.1,0.1],'string',mystr3,...
        'interpreter','latex', 'FontSize',14,'LineStyle','none',...
        'FontWeight','bold');    
    annotation(gcf,'textbox', [0.02,0.47,0.1,0.1],'string',mystr4,...
        'interpreter','latex', 'FontSize',14,'LineStyle','none',...
        'FontWeight','bold'); 
end

% Update handles structure
guidata(hObject, handles);



%% NEXT BUTTON
% * Executes on button press in |pushbutton_next|;
% * It recovers all data provided by the user in the GUI fields;
% * It checks for relevant (i.e. mesh) changes as compared to the previous |mat| file. If
%   such changes exist, it deletes the |mat| file corresponding to the next
%   GUIs to force their definition from scratch. If no mesh changes were
%   detected, the next GUI will load its previous version;
% * If the user asked for the model to be saved, it saves the information
%   regarding this GUI in the specified file and folder.
function pushbutton_next_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton_next (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

%%
% Reading the information provided by the user in the GUI fields.
UI.EdgesOrder = str2double(get(handles.edit_OrderEdge,'String'));
UI.NumberGaussPoints = str2double(get(handles.edit_NGP,'String'));
UI.MeshOption = get(handles.popupmenu_mesh,'Value');
UI.SolMethod = get(handles.popupmenu_SolMethod,'Value');
UI.EquationType = get(handles.popupmenu_EquationType,'Value');
UI.ProblemType = get(handles.popupmenu_ProblemType,'Value');
UI.offset = str2double(get(handles.edit_offset,'String'));
UI.lambda = str2double(get(handles.edit_lambda,'String'));
UI.k = str2double(get(handles.edit_k,'String'));

UI.incident.amplitude = str2double(get(handles.edit_incident_amplitude,'String'));
UI.incident.angle = str2double(get(handles.edit_incident_angle,'String'));


%% 
% Checking if the user forgot to define lambda in the MFS
if UI.SolMethod == 2 && isnan(UI.lambda)
    errordlg('In the Direct MFS, you must enter a real positive value for lambda','Invalid input','modal');
    return;
end

% Checking if the user forgot to define k for Helmholtz problem
if UI.EquationType == 2 && ( isnan(UI.k) || UI.k == 0 )
    errordlg('For Helmholtz problem, you must enter a real nonzero value for wave number','Invalid input','modal');
    return;
end

% Checking if the user chose both "exterior" ProblemType AND regular MeshOption
if UI.MeshOption == 1 && UI.ProblemType == 2 
    errordlg('For exterior problems, only non-regular meshes are allowed with external boundaries treated as limits of solution plot','Invalid input','modal');
    return;
end

% Checking if the user forgot to define incident wave amplitude for Helmholtz problem
if UI.EquationType == 2 && UI.ProblemType==2 && ( isnan(UI.incident.amplitude) ) % || UI.incident.amplitude == 0 )
    errordlg('For Helmholtz problem, you must enter a real value for an amplitude of incident wave','Invalid input','modal');
    return;
end

% Checking if the user forgot to define incident wave angle for Helmholtz problem
if UI.EquationType == 2 && UI.ProblemType==2 && ( isnan(UI.incident.angle)) % || UI.incident.angle == 0 )
    errordlg('For Helmholtz problem, you must enter a real value for an angle of incident wave','Invalid input','modal');
    return;
end


%%
% If the user asked for the model to be saved, it stores the path and the
% file name.
if isfield(handles,'DirName')
    DirName = handles.DirName;
    FileName = handles.FileName;
else
    DirName = '';
    FileName = '';
end

%%
% *Procedure for the regular rectangular mesh*
if UI.MeshOption == 1
    
    %%
    % Reading mesh information
    UI.L = str2double(get(handles.edit_DimX,'String'));
    UI.B = str2double(get(handles.edit_DimY,'String'));
    UI.Nx = str2double(get(handles.edit_NLoopX,'String'));
    UI.Ny = str2double(get(handles.edit_NLoopY,'String'));
    
   
    %%
    % |p|, |e| and |t| variables are allocated dummy values to avoid errors
    % related to the comparison between files corresponding to
    % regular (rectangular) and triangular meshes.
    p = 0; e = 0; t = 0;
    
    %%
    % Check if critical changes were made in the current GUI session. Note
    % that critical changes only refer to the mesh, not necessarily to the
    % geometry of the structure.
    if exist('./dBMT_StructDef.mat','file')
        NewData = struct('UI',UI,'p',p,'t',t); % temporary structures to detect critical changes
        OldData=load('dBMT_StructDef.mat','UI','p','t');
        % Checks the NewData and the OldData files
        % for changes in the critical fields. ChangesQ = 1 if there are
        % changes, 0 otherwise.
        if isequal(NewData.p, OldData.p) &&...
                isequal(NewData.t, OldData.t) && ...
                isequal(NewData.UI.Nx,OldData.UI.Nx) && ...
                isequal(NewData.UI.Ny,OldData.UI.Ny) && ...
                NewData.UI.ProblemType == OldData.UI.ProblemType &&...
                NewData.UI.MeshOption == OldData.UI.MeshOption
            ChangesQ = 0;
        else
            ChangesQ = 1;
        end
        % deleting the auxiliary file
        clear NewData OldData;
    else
        % If there is no |dBMT_StructDef| file in the first place, it sets
        % ChangesQ to 1.
        ChangesQ = 1;
    end
    
    %%
    % Saving the workspace to the local |mat| file. If requested by the
    % user, the GUI information is also saved to the file specified by the
    % user.
    save('dBMT_StructDef','UI', 'p','e','t',...
        'DirName','FileName','ChangesQ');
    
    
    % If the folder where it looks for pre-load files is different from the
    % current folder, it creates a copy of dBMT_StructDef in the former
    if  ~strcmp(pwd,handles.WorkingFolder)
        save(fullfile(handles.WorkingFolder,'dBMT_StructDef'),'UI', ...
            'DirName','FileName', 'ChangesQ') ;
    end

    % Saves to the save file
    if ~isempty(DirName)
        save(fullfile(DirName,FileName),...
            'UI','p','e','t','ChangesQ');
    end
    
    %%
    % Closing the GUI
    close(handles.figure1);
    
    %%
    % If there are relevant changes, it physically deletes the |mat| files
    % associated to the following GUIs.
    if ChangesQ
        delete('dBMT_BC1.mat','dBMT_BC2.mat');
    end

    dBMT_BC1;

    %%
    % *Procedure for the automatic triangular mesh*
else
    
    %%
    % Reading mesh information. This data is useless for the automatic mesh
    % generation. It is only stored to fill in the corresponding fields in
    % a future run.
    UI.L = str2double(get(handles.edit_DimX,'String'));
    UI.B = str2double(get(handles.edit_DimY,'String'));
    UI.Nx = str2double(get(handles.edit_NLoopX,'String'));
    UI.Ny = str2double(get(handles.edit_NLoopY,'String'));

    %%
    % Launching |pdetool| to define the mesh
    pdewindow = pdeinit; 
    %% 
    % Stopping the execution until the |pdetool| window is closed 
    waitfor(pdewindow);
    
    %%
    % This is a basic check to confirm that the user saved the mesh 
    % information. A new mesh need not be created if the corresponding
    % information already exist in |dBMT_StructDef.mat|, so the program checks
    % if a new mesh was defined or if |dBMT_StructDef.mat| exists. Of course,
    % the check fails if |dBMT_StructDef.mat| exists, but does not contain
    % relevant mesh information (the program exists with an error).
    BaseVars = evalin('base','whos'); % collects variable info from base
    % if the mesh variables are not found in base and an old definition
    % does not exist in dBMT_StructDef.mat
    if (~ismember('p',[BaseVars(:).name]) || ~ismember('e',[BaseVars(:).name])) && ...
            (~exist('./dBMT_StructDef.mat','file'))
        errordlg('No mesh information was exported or variable names were changed. Please press "Next" again and export the mesh info as p, e and t.','Invalid input','modal');
        uicontrol(hObject);
        return;
    end
    
    %%
    % if the mesh variables are found in base, it loads them...
    if ismember('p',[BaseVars(:).name]) && ismember('e',[BaseVars(:).name])...
            && ismember('t',[BaseVars(:).name])
        p = evalin('base','p');
        e = evalin('base','e');
        t = evalin('base','t');
    %%
    % ... otherwise, it loads them from the dBMT_StructDef.mat
    else
        load('dBMT_StructDef','p','e','t');
    end
    
    %%
    % Check if critical changes were made in the current GUI session. Note
    % that critical changes only refer to the mesh, not necessarily to the
    % geometry of the structure.
    if exist('./dBMT_StructDef.mat','file')
        NewData = struct('UI',UI,'p',p,'t',t); % temporary structures to detect critical changes
        OldData=load('dBMT_StructDef.mat','UI','p','t');
        % Checks the NewData and the OldData files
        % for changes in the critical fields. ChangesQ = 1 if there are
        % changes, 0 otherwise.
        if isequal(NewData.p, OldData.p) &&...
                isequal(NewData.t, OldData.t) && ...
                NewData.UI.ProblemType == OldData.UI.ProblemType &&...
                NewData.UI.MeshOption == OldData.UI.MeshOption
            ChangesQ = 0;
        else
            ChangesQ = 1;
        end
        % deleting the auxiliary file
        clear NewData OldData;
    else
        % If there is no |dBMT_StructDef| file in the first place, it sets
        % ChangesQ to 1.
        ChangesQ = 1;
    end
    
    %%
    % Saving the workspace to the local |mat| file. If requested by the
    % user, the GUI information is also saved to the file specified by the
    % user.
    save('dBMT_StructDef','UI','p','e','t','DirName','FileName','ChangesQ');



    % If the folder where it looks for pre-load files is different from the
    % current folder, it creates a copy of dBMT_StructDef in the former
    if  ~strcmp(pwd,handles.WorkingFolder)
        save(fullfile(handles.WorkingFolder,'dBMT_StructDef'),...
            'UI','p','e','t', 'DirName','FileName','ChangesQ');
    end
    
    % Saves to the save file
    if ~isempty(DirName)       
        save(fullfile(DirName,FileName),...
            'UI','p','e','t','ChangesQ');        
    end
    
    
    %%
    % Closing the GUI
    close(handles.figure1);
    
    %%
    % If there are relevant changes, it physically deletes the |mat| files
    % associated to the following GUIs.
    if ChangesQ
        delete('dBMT_BC1.mat','dBMT_BC2.mat');
    end

    dBMT_BC1;
    
end


%% RESET BUTTON
% * Executes on button press in |pushbutton_reset|;
% * It deletes all entries of the edit fields and resets the pop-up menus;
% * It also deletes the save information so that the save button action is
% reversed.
function pushbutton_reset_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton_reset (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
set(handles.edit_DimX,'String','');
set(handles.edit_DimY,'String','');
set(handles.edit_NLoopX,'String','');
set(handles.edit_NLoopY,'String','');
set(handles.edit_OrderEdge,'String','');
set(handles.edit_NGP,'String','');
set(handles.edit_offset,'String','');
set(handles.edit_lambda,'String','NaN');
set(handles.edit_Path,'String','');
set(handles.popupmenu_mesh,'Value',1);
set(handles.popupmenu_SolMethod,'Value',1);
set(handles.popupmenu_EquationType,'Value',1);
set(handles.popupmenu_ProblemType,'Value',1);

set(handles.edit_k,'String','');

set(handles.edit_incident_amplitude,'String','');
set(handles.edit_incident_angle,'String','');


%%
% Reseting |edit_Path|, the pop-up menus, and the properties of the mesh
% definition fields
set(handles.edit_Path,'String',...
    'THE MODEL WILL NOT BE SAVED!  Please press Save if you wish to save the model.',...
    'BackgroundColor',[1.0 0.694 0.392]);

set(handles.edit_DimX, 'enable', 'on');
set(handles.edit_DimY, 'enable', 'on');
set(handles.edit_NLoopX, 'enable', 'on');
set(handles.edit_NLoopY, 'enable', 'on');
set(handles.text_lambda, 'visible', 'off');
set(handles.edit_lambda, 'visible', 'off');
set(handles.edit_k, 'visible', 'off');
set(handles.text_k, 'visible', 'off');
% set(handles.edit_k,'String','');
set(handles.text_ext_Helmholtz_params_caption, 'visible', 'off');  
set(handles.edit_incident_amplitude, 'visible', 'off');
set(handles.text_incident_amplitude, 'visible', 'off');
set(handles.edit_incident_angle, 'visible', 'off');
set(handles.text_incident_angle, 'visible', 'off');


handles.FileName = '';
handles.DirName = '';

delete(handles.A1);
mystr = '$$\nabla^2 u \left( x,y \right) = 0 $$';

% writing the current equation type on the interface
try
    A1 = annotation(handles.uipanel2,'textbox', [0.02,0.71,0.1,0.1],'string',mystr,...
        'interpreter','latex', 'FontSize',14,'LineStyle','none',...
        'FontWeight','bold');
catch errors
    A1 = annotation(gcf,'textbox', [0.02,0.71,0.1,0.1],'string',mystr,...
        'interpreter','latex', 'FontSize',14,'LineStyle','none',...
        'FontWeight','bold');
end

% storing the A1 annotation in handles to make it accessible
handles.A1 = A1;

%%
% Updating the |handles| structure
guidata(hObject, handles);


%% SAVE BUTTON
% * Executes on button press in |Save|;
% * Reads the path and filename provided by the user to save the model;
% * Generates the |FileName| and |DirName| members in the |handles|
% structure to store this information and to make it available to other
% functions.
function Save_Callback(hObject, eventdata, handles)
% hObject    handle to Save (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

%% 
% Reads the |handles| structure
handles=guidata(hObject);

%%
% Gets the path and filename to save the model
[FileName,DirName] = uiputfile('*.mat','Save as');

if FileName
    %%
    % Registers the path to the save file in the |edit_Path| box
    set(handles.edit_Path,'string',fullfile(DirName,FileName),'BackgroundColor',[0 1 0]);
    
    %%
    % Generates the |FileName| and |DirName| members in the |handles|
    % structure to store this information and to make it available to other
    % functions.
    handles.FileName = FileName;
    handles.DirName = DirName;
    
    %%
    % If user cancelled the saving process and no save file was given
else
    handles.FileName = '';
    handles.DirName = '';    
end

%%
% Updating the |handles| structure
guidata(hObject, handles);



%% LOAD BUTTON
% * Executes on button press in |Load|;
% * Loads all relevant variables in the file appointed by the user into the
% workspace. If the definition of the model was not completed in the
% previous session, some of the variables cannot be loaded (the
% corresponding warning is suppressed);
% * Stores the required variables into the dBMT_StructDef |mat| file.
% This data must always be available;
% * Verifies if the data generated by the other three GUIs are
% available. If they are, stores them into the local |mat| files, otherwise
% _deletes_ the |mat| files, to force their reinitialization;
% * Deletes the |FileName| and |DirName| variables to avoid overwriting of the
% loaded file;
% * Refreshes the interface, filling in the fields with the newly loaded
% values.
function Load_Callback(hObject, eventdata, handles)
% hObject    handle to Load (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


%%
% Reads the |handles| structure
handles=guidata(hObject);

%%
% Loads the |mat| file indicated by the user
[FileName,DirName] = uigetfile('*.mat','File to load');

%%
% Deletes the |FileName| and |DirName| variables to avoid overwriting;
handles.FileName = '';
handles.DirName = '';

%%
% Loading all relevant variables into the current workspace
load(fullfile(DirName,FileName),'UI','p','e','t','ChangesQ','data',...
    'dataDir','dataNeu','dataRobin',...
    'edgesDirichlet','edgesNeumann','edgesRobin');

%%
% Saving the local |dBMT_StructDef.mat| file
save('dBMT_StructDef','UI','p','e','t','ChangesQ');

% If the folder where it looks for pre-load files is different from the
% current folder, it creates a copy of StructDef in the former
if  ~strcmp(pwd,handles.WorkingFolder)
    save(fullfile(handles.WorkingFolder,'dBMT_StructDef'),'UI',...
        'p','e','t','ChangesQ');
end


%%
% If 'p' exists (is not zero), updates 'p' and 'e' in the base workspace
if any(any(p~=0))
    assignin('base','p',p);
    assignin('base','e',e);
    assignin('base','t',t);
end

%%
% Depending on the kind of mesh that is selected ...
if UI.MeshOption == 1 % regular rectangular mesh
    %%
    % ... checks if the variable created by the second GUI exists and if it
    % doesn't, it deletes the local |mat| file to force reinitialization...
    if ~exist('data','var')
        delete('dBMT_BC1.mat');
        %%
        % ... or it stores (and overwrites) the |mat| file if the variable
        % exists.
    else
        save('dBMT_BC1','data');
    end
    
    %%
    % ... checks if the variables created by the third GUI exist and if they
    % don't, it deletes the local |mat| file to force reinitialization...
    if ~exist('dataDir','var') && ~exist('dataNeu','var') && ~exist('dataRobin','var')
        delete('dBMT_BC2.mat');
        %%
        % ... or it stores (and overwrites) the |mat| file if the variables
        % exiUI.
    else
        delete('dBMT_BC2.mat');
        dummy = 0;
        save('dBMT_BC2','dummy');
        if exist('dataDir','var')
            save('dBMT_BC2','-append','edgesDirichlet','dataDir');
        end
        if exist('dataNeu','var')
            save('dBMT_BC2','-append','edgesNeumann','dataNeu');
        end
        if exist('dataRobin','var')
            save('dBMT_BC2','-append','edgesRobin','dataRobin');
        end
    end
    
    %%
    % Same operation for the irregular mesh file.
else
    if ~exist('data','var')
        delete('dBMT_BC1.mat');
    else
        save('dBMT_BC1','data');
    end
    
    if ~exist('dataDir','var') && ~exist('dataNeu','var') && ~exist('dataRobin','var')
        delete('dBMT_BC2.mat');
    else
        delete('dBMT_BC2.mat');
        dummy = 0;
        save('dBMT_BC2','dummy');
        if exist('dataDir','var')
            save('dBMT_BC2','-append','edgesDirichlet','dataDir');
        end
        if exist('dataNeu','var')
            save('dBMT_BC2','-append','edgesNeumann','dataNeu');
        end
        if exist('dataRobin','var')
            save('dBMT_BC2','-append','edgesRobin','dataRobin');
        end
    end
end

%%
% Refreshes the interface, writing the loaded values into its fields...
set(handles.edit_DimX,'String',sprintf('%d',UI.L));
set(handles.edit_DimY,'String',sprintf('%d',UI.B));
set(handles.edit_NLoopX,'String',sprintf('%d',UI.Nx));
set(handles.edit_NLoopY,'String',sprintf('%d',UI.Ny));
set(handles.edit_OrderEdge,'String',sprintf('%d',UI.EdgesOrder));
set(handles.edit_NGP,'String',sprintf('%d',UI.NumberGaussPoints));
set(handles.edit_offset,'String',sprintf('%g',UI.offset));
set(handles.edit_lambda,'String',sprintf('%g',UI.lambda));
set(handles.popupmenu_mesh,'Value',UI.MeshOption);
set(handles.popupmenu_SolMethod,'Value',UI.SolMethod);
set(handles.popupmenu_EquationType,'Value',UI.EquationType);

set(handles.edit_incident_amplitude,'String',sprintf('%g',UI.incident.amplitude));
set(handles.edit_incident_angle,'String',sprintf('%g',UI.incident.angle));

if isnan(UI.k)
    set(handles.edit_k,'String',sprintf(''));
else
    if imag(UI.k) == 0
        set(handles.edit_k,'String',sprintf('%g',UI.k));
    elseif real(UI.k) == 0
        set(handles.edit_k,'String',sprintf('%gi',imag(UI.k) ));
    else
        set(handles.edit_k,'String',sprintf('%g+%gi',real(UI.k), imag(UI.k) ));
    end
end

set(handles.edit_Path,'string',...
    'THE MODEL WILL NOT BE SAVED!  Please press Save if you wish to save the model.',...
    'BackgroundColor',[1.0 0.694 0.392]);

%%
% ... and activates or inactivates the regular mesh fields according to the
% type of mesh in the loaded model.
if UI.MeshOption == 1
    set(handles.edit_DimX, 'enable', 'on');
    set(handles.edit_DimY, 'enable', 'on');
    set(handles.edit_NLoopX, 'enable', 'on');
    set(handles.edit_NLoopY, 'enable', 'on');
else
    set(handles.edit_DimX, 'enable', 'off');
    set(handles.edit_DimY, 'enable', 'off');
    set(handles.edit_NLoopX, 'enable', 'off');
    set(handles.edit_NLoopY, 'enable', 'off');
end
% activating or inactivating the lambda field according to the solution
% method
if UI.SolMethod == 2 % dMFS
    set(handles.edit_lambda, 'visible', 'on');
    set(handles.text_lambda, 'visible', 'on');
else
    set(handles.edit_lambda, 'visible', 'off');
    set(handles.text_lambda, 'visible', 'off');
end
% activating or inactivating the wave number field according to the
% equation type. Also, the text explaining the equation type is updated
delete(handles.A1);
if UI.EquationType == 2
    % if EquationType = 2, that is, the problem to solve is the Helmholtz
    % equation, the k field becomes visible and the correct equation
    % decription is saved
    set(handles.edit_k, 'visible', 'on');
    set(handles.text_k, 'visible', 'on');
    % writing the equation type info into the GUI
    mystr = '$$ \nabla^2 u \left( x,y \right) + k^2 \cdot u \left( x,y \right) = 0 $$';
else
    % if EquationType = 1, that is, the problem to solve is Laplace
    % equation, the k field becomes invisible and the correct equation
    % decription is saved
    set(handles.edit_k, 'visible', 'off');
    set(handles.text_k, 'visible', 'off');
    mystr = '$$\nabla^2 u \left( x,y \right) = 0 $$';
end

if UI.EquationType == 2 && UI.ProblemType == 2
    % if EquationType = 2, that is, the problem to solve is the Helmholtz
    % equation, the k field becomes visible and the correct equation
    % decription is saved
    set(handles.text_ext_Helmholtz_params_caption, 'visible', 'on');  
    set(handles.edit_incident_angle, 'visible', 'on');
    set(handles.text_incident_angle, 'visible', 'on');
    set(handles.edit_incident_amplitude, 'visible', 'on');
    set(handles.text_incident_amplitude, 'visible', 'on');    
    
else
    % if EquationType = 1, that is, the problem to solve is Laplace
    % equation, the k field becomes invisible and the correct equation
    % decription is saved
    set(handles.text_ext_Helmholtz_params_caption, 'visible', 'off');  
    set(handles.edit_k, 'visible', 'off');
    set(handles.text_k, 'visible', 'off');
    set(handles.edit_incident_angle, 'visible', 'off');
    set(handles.text_incident_angle, 'visible', 'off');  
    set(handles.edit_incident_amplitude, 'visible', 'off');
    set(handles.text_incident_amplitude, 'visible', 'off');
    
end




% writing the current equation type on the interface
try
    A1 = annotation(handles.uipanel2,'textbox', [0.02,0.71,0.1,0.1],'string',mystr,...
        'interpreter','latex', 'FontSize',14,'LineStyle','none',...
        'FontWeight','bold');
catch errors
    A1 = annotation(gcf,'textbox', [0.02,0.71,0.1,0.1],'string',mystr,...
        'interpreter','latex', 'FontSize',14,'LineStyle','none',...
        'FontWeight','bold');
end

% storing the A1 annotation in handles to make it accessible
handles.A1 = A1;

%%
% Updates the |handles| structure
guidata(hObject, handles);



%% CLEAR BUTTON
% * Executes on button press in |Clear|;
% * Deletes the path and name of the save file and reinitializes the
% |edit_Path| field.
function Clear_Callback(hObject, eventdata, handles)
% hObject    handle to Clear (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

%%
% Reads the |handles| structure
handles=guidata(hObject);

%%
% Deletes the |FileName| and |DirName| variables
handles.FileName = '';
handles.DirName = '';
set(handles.edit_Path,'string',...
    'THE MODEL WILL NOT BE SAVED!  Please press Save if you wish to save the model.',...
    'BackgroundColor',[1.0 0.694 0.392]);

%%
% Updates the |handles| structure
guidata(hObject, handles);


%% POP-UP MENUS

% --- Executes on selection change in popupmenu_mesh.
function popupmenu_mesh_Callback(hObject, eventdata, handles)
% hObject    handle to popupmenu_mesh (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns popupmenu_mesh contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popupmenu_mesh
if get(handles.popupmenu_mesh,'Value') == 1
    set(handles.edit_DimX, 'enable', 'on');
    set(handles.edit_DimY, 'enable', 'on');
    set(handles.edit_NLoopX, 'enable', 'on');
    set(handles.edit_NLoopY, 'enable', 'on');
else
   set(handles.edit_DimX, 'enable', 'off');
   set(handles.edit_DimY, 'enable', 'off');
   set(handles.edit_NLoopX, 'enable', 'off');
   set(handles.edit_NLoopY, 'enable', 'off');
end

% --- Executes during object creation, after setting all properties.
function popupmenu_mesh_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popupmenu_mesh (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in popupmenu_SolMethod.
function popupmenu_SolMethod_Callback(hObject, eventdata, handles)
% hObject    handle to popupmenu_SolMethod (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns popupmenu_SolMethod contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popupmenu_SolMethod
if get(handles.popupmenu_SolMethod,'Value') == 2
    set(handles.edit_lambda, 'visible', 'on');
    set(handles.text_lambda, 'visible', 'on');
else
    set(handles.edit_lambda, 'visible', 'off');
    set(handles.text_lambda, 'visible', 'off');
end

% --- Executes during object creation, after setting all properties.
function popupmenu_SolMethod_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popupmenu_SolMethod (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% --- Executes on selection change in popupmenu_EquationType.
function popupmenu_EquationType_Callback(hObject, eventdata, handles)
% hObject    handle to popupmenu_EquationType (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns popupmenu_EquationType contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popupmenu_EquationType

delete(handles.A1);

if get(handles.popupmenu_EquationType,'Value') == 2
    % if EquationType = 2, that is, the problem to solve is the Helmholtz
    % equation, the k field becomes visible and the correct equation 
    % decription is saved
    set(handles.edit_k, 'visible', 'on');
    set(handles.text_k, 'visible', 'on');
    % writing the equation type info into the GUI
    mystr = '$$ \nabla^2 u \left( x,y \right) + k^2 \cdot u \left( x,y \right) = 0 $$';
else
    % if EquationType = 1, that is, the problem to solve is Laplace
    % equation, the k field becomes invisible 
    % and the correct equation decription is saved
    set(handles.edit_k, 'visible', 'off');
    set(handles.text_k, 'visible', 'off');
    mystr = '$$\nabla^2 u \left( x,y \right) = 0 $$'; 
end

if get(handles.popupmenu_EquationType,'Value') == 2 && ...
    get(handles.popupmenu_ProblemType,'Value') == 2
    % if ProblemType = 2, that is, the problem to solve is exterior
    % problem, the incidentWaveAngle field becomes visible 
    set(handles.text_ext_Helmholtz_params_caption, 'visible', 'on');  
    set(handles.edit_incident_amplitude, 'visible', 'on');
    set(handles.text_incident_amplitude, 'visible', 'on');      
    set(handles.edit_incident_angle, 'visible', 'on');
    set(handles.text_incident_angle, 'visible', 'on');  
else
    % if ProblemType = 1, that is, the problem to solve is interior
    % problem, the incidentWaveAngle field becomes invisible 
    set(handles.text_ext_Helmholtz_params_caption, 'visible', 'off');  
    set(handles.edit_incident_amplitude, 'visible', 'off');
    set(handles.text_incident_amplitude, 'visible', 'off');   
    set(handles.edit_incident_angle, 'visible', 'off');
    set(handles.text_incident_angle, 'visible', 'off');   
end


% writing the current equation type on the interface
try
    A1 = annotation(handles.uipanel2,'textbox', [0.02,0.71,0.1,0.1],'string',mystr,...
        'interpreter','latex', 'FontSize',14,'LineStyle','none',...
        'FontWeight','bold');
catch errors
    A1 = annotation(gcf,'textbox', [0.02,0.71,0.1,0.1],'string',mystr,...
        'interpreter','latex', 'FontSize',14,'LineStyle','none',...
        'FontWeight','bold');
end

% storing the A1 annotation in handles to make it accessible
handles.A1 = A1;

% Update handles structure
guidata(hObject, handles);

% --- Executes during object creation, after setting all properties.
function popupmenu_EquationType_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popupmenu_EquationType (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% --- Executes on selection change in popupmenu_ProblemType.
function popupmenu_ProblemType_Callback(hObject, eventdata, handles)
% hObject    handle to popupmenu_ProblemType (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns popupmenu_ProblemType contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popupmenu_ProblemType
% delete(handles.A1);

if get(handles.popupmenu_EquationType,'Value') == 2 && ...
    get(handles.popupmenu_ProblemType,'Value') == 2
    % if ProblemType = 2, that is, the problem to solve is exterior
    % problem, the incidentWaveAngle field becomes visible 
    set(handles.text_ext_Helmholtz_params_caption, 'visible', 'on');  
    set(handles.edit_incident_amplitude, 'visible', 'on');
    set(handles.text_incident_amplitude, 'visible', 'on');      
    set(handles.edit_incident_angle, 'visible', 'on');
    set(handles.text_incident_angle, 'visible', 'on');  
   
else
    % if ProblemType = 1, that is, the problem to solve is interior
    % problem, the incidentWaveAngle field becomes invisible 
    set(handles.text_ext_Helmholtz_params_caption, 'visible', 'off');  
    set(handles.edit_incident_amplitude, 'visible', 'off');
    set(handles.text_incident_amplitude, 'visible', 'off');       
    set(handles.edit_incident_angle, 'visible', 'off');
    set(handles.text_incident_angle, 'visible', 'off');   
end


% Update handles structure
guidata(hObject, handles);

% --- Executes during object creation, after setting all properties.
function popupmenu_ProblemType_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popupmenu_ProblemType (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



%% EDIT FIELDS
% Field box editing functions. Most Callbacks check for the validity of the
% data.


function edit_DimX_Callback(hObject, eventdata, handles)
% hObject    handle to edit_DimX (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit_DimX as text
%        str2double(get(hObject,'String')) returns contents of edit_DimX as a double
L = str2double(get(hObject,'String'));
if isnan(L) || ~isreal(L) || isequal(L,0) || L < 0
    set(hObject,'String','');
    errordlg('You must enter a real positive value','Invalid input','modal');
    uicontrol(hObject);
    return;
end

% --- Executes during object creation, after setting all properties.
function edit_DimX_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit_DimX (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function edit_DimY_Callback(hObject, eventdata, handles)
% hObject    handle to edit_DimY (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit_DimY as text
%        str2double(get(hObject,'String')) returns contents of edit_DimY as a double
B = str2double(get(hObject,'String'));
if isnan(B) || ~isreal(B) ||  isequal(B,0) || B < 0
    set(hObject,'String','');
    errordlg('You must enter a real positive value','Invalid input','modal');
    uicontrol(hObject);
    return;
end

% --- Executes during object creation, after setting all properties.
function edit_DimY_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit_DimY (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function edit_NLoopX_Callback(hObject, eventdata, handles)
% hObject    handle to edit_NLoopX (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit_NLoopX as text
%        str2double(get(hObject,'String')) returns contents of edit_NLoopX as a double
Nx = str2double(get(hObject,'String'));
if isnan(Nx) || ~isreal(Nx) || logical(abs(round(Nx)-Nx)<eps)==0 || isequal(Nx,0) || Nx < 0
    set(hObject,'String','');
    errordlg('You must enter a positive integer value','Invalid input','modal');
    uicontrol(hObject);
    return;
end

% --- Executes during object creation, after setting all properties.
function edit_NLoopX_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit_NLoopX (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function edit_NLoopY_Callback(hObject, eventdata, handles)
% hObject    handle to edit_NLoopY (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit_NLoopY as text
%        str2double(get(hObject,'String')) returns contents of edit_NLoopY as a double
Ny = str2double(get(hObject,'String'));
if isnan(Ny) || ~isreal(Ny) || logical(abs(round(Ny)-Ny)<eps)==0 || isequal(Ny,0) || Ny < 0
    set(hObject,'String','');
    errordlg('You must enter a positive integer value','Invalid input','modal');
    uicontrol(hObject);
    return;
end

% --- Executes during object creation, after setting all properties.
function edit_NLoopY_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit_NLoopY (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function edit_OrderEdge_Callback(hObject, eventdata, handles)
% hObject    handle to edit_OrderEdge (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit_OrderEdge as text
%        str2double(get(hObject,'String')) returns contents of edit_OrderEdge as a double
EdgesOrder = str2double(get(handles.edit_OrderEdge,'String'));
if isnan(EdgesOrder) || ~isreal(EdgesOrder) || ...
        logical(abs(round(EdgesOrder)-EdgesOrder)<eps)==0 || EdgesOrder < 0
    set(hObject,'String','');
    errordlg('You must enter either 0 or a natural number','Invalid input','modal');
    uicontrol(hObject);
    return;
end

% --- Executes during object creation, after setting all properties.
function edit_OrderEdge_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit_OrderEdge (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit_NGP_Callback(hObject, eventdata, handles)
% hObject    handle to edit_NGP (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit_NGP as text
%        str2double(get(hObject,'String')) returns contents of edit_NGP as a double
NumberGaussPoints = str2double(get(handles.edit_NGP,'String'));
if isnan(NumberGaussPoints) || ~isreal(NumberGaussPoints) || ...
        logical(abs(round(NumberGaussPoints)-NumberGaussPoints)<eps)==0 ||...
        isequal(NumberGaussPoints,0) || NumberGaussPoints < 0
    set(hObject,'String','');
    errordlg('You must enter a positive integer value','Invalid input','modal');
    uicontrol(hObject);
    return;
end

% --- Executes during object creation, after setting all properties.
function edit_NGP_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit_NGP (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function edit_Path_Callback(hObject, eventdata, handles)
% hObject    handle to edit_Path (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit_Path as text
%        str2double(get(hObject,'String')) returns contents of edit_Path as a double

% --- Executes during object creation, after setting all properties.
function edit_Path_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit_Path (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function edit_offset_Callback(hObject, eventdata, handles)
% hObject    handle to edit_offset (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit_offset as text
%        str2double(get(hObject,'String')) returns contents of edit_offset as a double
offset = str2double(get(handles.edit_offset,'String'));
if isnan(offset) || ~isreal(offset) || offset < 0 || offset > 0.4 
    set(hObject,'String','');
    errordlg('You must enter a real value between 0.0 and 0.4','Invalid input','modal');
    uicontrol(hObject);
    return;
end


% --- Executes during object creation, after setting all properties.
function edit_offset_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit_offset (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function edit_lambda_Callback(hObject, eventdata, handles)
% hObject    handle to edit_lambda (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit_lambda as text
%        str2double(get(hObject,'String')) returns contents of edit_lambda as a double
lambda = str2double(get(handles.edit_offset,'String'));
if isnan(lambda) || ~isreal(lambda) || lambda <= 0 
    set(hObject,'String','NaN');
    errordlg('You must enter a real positive value','Invalid input','modal');
    uicontrol(hObject);
    return;
end

% --- Executes during object creation, after setting all properties.
function edit_lambda_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit_lambda (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function edit_k_Callback(hObject, eventdata, handles)
% hObject    handle to edit_k (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit_k as text
%        str2double(get(hObject,'String')) returns contents of edit_k as a double
k = str2double(get(handles.edit_k,'String'));

if isnan(k) || ~isnumeric(k) || k == 0 
    set(hObject,'String','NaN');
    errordlg('You must enter a nonzero real or complex value','Invalid input','modal');
    uicontrol(hObject);
    return;
end

% --- Executes during object creation, after setting all properties.
function edit_k_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit_k (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function edit_incident_amplitude_Callback(hObject, eventdata, handles)
% hObject    handle to edit_incident_amplitude (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit_incident_amplitude as text
%        str2double(get(hObject,'String')) returns contents of edit_incident_amplitude as a double
incident_amplitude = str2double(get(handles.edit_incident_amplitude,'String'));
if isnan(incident_amplitude) || isempty(incident_amplitude || incident_amplitude==0 )
    set(hObject,'String','NaN');
    errordlg('You must enter a real positive value','Invalid input','modal');
    uicontrol(hObject);
    return;
end

% --- Executes during object creation, after setting all properties.
function edit_incident_amplitude_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit_incident_amplitude (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function edit_incident_angle_Callback(hObject, eventdata, handles)
% hObject    handle to edit_incident_angle (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit_incident_angle as text
%        str2double(get(hObject,'String')) returns contents of edit_incident_angle as a double
incident_angle = str2double(get(handles.edit_incident_angle,'String'));
if isnan(incident_angle) || isempty(incident_angle)
    set(hObject,'String','NaN');
    errordlg('You must enter a real value','Invalid input','modal');
    uicontrol(hObject);
    return;
end

% --- Executes during object creation, after setting all properties.
function edit_incident_angle_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit_incident_angle (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


%% TEXT FIELDS
% Text fields' editing functions. The respective |ButtonDownFcn| defines
% the help of that field, accessed through right clicking.


% --- Executes during object creation, after setting all properties.
function text_DimX_CreateFcn(hObject, eventdata, handles)
% hObject    handle to text_DimX (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called


% --- Executes during object creation, after setting all properties.
function text_DimY_CreateFcn(hObject, eventdata, handles)
% hObject    handle to text_DimY (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called


% --- Executes during object creation, after setting all properties.
function text_NLoopX_CreateFcn(hObject, eventdata, handles)
% hObject    handle to text_NLoopX (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called


% --- Executes during object creation, after setting all properties.
function text_NLoopY_CreateFcn(hObject, eventdata, handles)
% hObject    handle to text_NLoopY (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called


% --- Executes during object creation, after setting all properties.
function text_OrderEdge_CreateFcn(hObject, eventdata, handles)
% hObject    handle to text_OrderEdge (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called


% --- Executes during object creation, after setting all properties.
function text_NGP_CreateFcn(hObject, eventdata, handles)
% hObject    handle to text_NGP (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called
