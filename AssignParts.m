function [Edges, Dim] = AssignParts(Edges)
% ASSIGNPARTS maps the solving system and assigns each boundary element an
% entry column and a dimension.
%
% ASSIGNPARTS is called by MAIN. 
% Input: the Edges structure. 
% Return: the Edges structure, updated with two fields containing the 
%  insertion columns of the respective blocks in the G and H matrices, 
%  and their dimensions. It also returns Dim, which lists the dimensions
%  of the G and H matrices (and of the solving system, in the end).
%
%
% BIBLIOGRAPHY
% 1. FreeHyTE GIT Page 
% 2. Borkowski M, Moldovan ID - Direct Boundary Method Toolbox for some
% elliptic PDEs in FreeHyTE framework, submitted to Advances in Engineering
% Software, 2019
%
% The layout of the H (or G) matrices that compose the solving system is
% presented below. 
%
%       <-------- Dim(2) --------->
%   _________________       _________       _
%   |       |       |       |       |       |
%   |       |       |       |       |       |
%   |   H   |   H   |  ...  |   H   |  ... Dim (1)
%   |    1  |    2  |       |    i  |       |
%   |       |       |       |       |       |
%   |_______|_______|       |_______|       _
%
%
% The blocks Gi/Hi belonging to boundary element i are inserted at column
% Edges.insert(i). Their number of columns is equal to the order of the
% boundary element basis, plus one Edges.dim(i) = Edges.order(i)+1
%
% The layout and storage of matrices G and H is discussed at length in
% reference [2].
% 

%% Initialization
% Initializes the current entry indicator, entry.
entry = 1;

% Initializes the insertion points for the boundary blocks
Edges.insert = zeros(length(Edges.type),1);

%% Mapping of the boundary entry blocks of the system
% The insertion point of the boundary block and its dimension are
% computed for each edge and stored in the Edges structure.
for ii= 1:length(Edges.insert)
    Edges.insert(ii)= entry;
    Edges.dim(ii) = Edges.order(ii)+1;
    entry = entry + Edges.dim(ii);
end
% Computing the total dimensions of the solving system
% The total number of columns is equal to the total number of interpolation
% nodes on all boundary elements
Dim(2) = entry-1;
% The number of lines is equal to the number of columns if the latter is
% odd, or plus one if it is not. The point is to keep the weighting basis
% complete.
Dim(1) = Dim(2) + ~mod(Dim(2),2);

end
