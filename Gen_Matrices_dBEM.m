function [G, H, R] = Gen_Matrices_dBEM(Edges, BConds, ...
        abscissa, weight, offset, preProcStruc, Helm_kappa)

% GEN_MATRICES_DBEM sweeps through the elements and calls the function
% that generate the H and G block matrices for direct Boundary
% Element Method

% Gen_GandH_Matrices_dBEM is called by MAIN***. 
% Input data: 
% the Edges and BConds -- structures that describe the problem
% abscissa, weights    - the Gauss-Legendre integration parameters
% offset               - the offset of the interpolation nodes of the boundary
%                          elements from their geometric limits
% preProcStruc         - auxiliary structure that stores collocation nodes,
%                         domain characteristic length, and optimal place for 
%                         Trefftz eigenexpension (domain barycenter)
%
% Helm_kappa           - if Helm_kappa == 0 => Laplace solver is invoked
%                        if Helm_kappa ~= 0 => Helmholtz solver is invoked

% Output/returns to MAIN***:
% G,H (and R if necessary) - matrices for dBEM ( Eqs.(15-17) in ref. [3])
%
% BIBLIOGRAPHY
% 1. Brebbia CA, Dominguez J, Boundary element methods for potential
% problems, Applied Mathematical Modelling, 1 (7), 372-378, 1977
% 2. Borkowski M, Moldovan ID - Direct Boundary Method Toolbox for some
%    elliptic PDEs in FreeHyTE framework, submitted to Advances in 
%    Engineering Software, 2019

% no of interpolation/collocation points = dimension of H and G square matrices
[N,~] = size(preProcStruc.collNodes) ;
    
% Initialization of the matrices

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Determining some details concerning Robin BC

% There is a need for calculating R vector only if Robin BC 
% du_dn = g(S) + ru
% is imposed at at least one boundary element _and_ function g(S) is
% non-zero there.

RVectorFLAG = 0 ; % flag that informs whether R vector is needed

if any(Edges.type == 'R') % if there is any element with Robin BC prescribed
    try
       gFunValues = cell2mat(BConds.Robin(:,1)) ;
       
       if ~all( ((gFunValues == 0)+isnan(gFunValues))==1 )
           % if there's at least one value of g function
           % that is (nonzero and (not NaN) )
           RVectorFLAG = 1 ;
       end

    catch ME 
       if (strcmp(ME.identifier,'MATLAB:catenate:dimensionMismatch'))
          % if the line 
          % gFunValues = cell2mat(BConds.Robin(:,1)) ;
          % throws an exception, probably the g function prescribed on 
          % one of the boundary elements is of higher degree
          % than zero -> vector R needs to be created
          RVectorFLAG = 1 ;      
       end
       %rethrow(ME)
    end 
end
    
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% preallocate space for final matrices
H = zeros(N);
G = zeros(N);
if RVectorFLAG
   R = zeros(N,1) ; 
else
   R = [] ;
end


%% Sweeping the edges
for ii=1:length(Edges.nini)
        % LocLoop is a structure where the features of the current
        % element which are directly useful for the calculation of the
        % local blocks of H and G matrices are stored.
        LocEdge =  struct('id',ii,'parametric',Edges.parametric(ii,:),...
            'order',Edges.order(ii),'insert',Edges.insert(ii),...
            'dim',Edges.dim(ii));

        % Computing the Gi and Hi matrices of element ii. GHR_Matrix_i is
        % a local function (see below).
        if RVectorFLAG && (~any(isnan(BConds.Robin{ii,1})) && ~all(BConds.Robin{ii,1}==0) ) 
            % if this is the element where g function is non-zero
            [Gi, Hi, Ri] = GHR_Matrix_i(LocEdge, abscissa, weight, offset, ...
                preProcStruc.collNodes, Helm_kappa, preProcStruc.dChL, BConds) ;
            % update R vector
            R = R + Ri ;
        else
            % if this is the element where g function does not exist
            [Gi, Hi] = GHR_Matrix_i(LocEdge, abscissa, weight, offset, ...
                preProcStruc.collNodes, Helm_kappa, preProcStruc.dChL ) ;
        end
            
        % Inserting the matrix in the global matrices. The insertion is made
        % at line & column LocEdge.insert(ii).
        G(:,LocEdge.insert:LocEdge.insert+LocEdge.dim-1) = Gi ;
        H(:,LocEdge.insert:LocEdge.insert+LocEdge.dim-1) = Hi ;
end

% enforcing diagonal values of H matrix -> ref. [1]
% H = H - diag(sum(H,2)) ;
% or even simpler approach since all the interpolation nodes 
% lie on the smooth parts of boundary:
H(sub2ind(size(H),1:size(H,1),1:size(H,2))) = 0.5 ;

end


function [Gi, Hi, vargout] = GHR_Matrix_i(LocEdge, abscissa, weight, offset, ...
                collNodes, Helm_kappa, dChL, BConds)
% GHR_Matrix_i is a local function that computes the G and H (and R?) 
% blocks of the LocEdge element. 
% The boundary elements are mapped to a [-1,1] interval to perform the integrations.
% The integrations are conducted with the use of the following 3D arrays:

%              . . . . .
%             . . . . .
%            /_/_/_/|.
%           /_/_/_/|/   quadrature nodes no (Q)
%          /_/_/_/|/
%         /_/_/_/|/
%  coll   |_|_|_|/  .
%  nodes  |_|_|_|/ . 
%  no     |_|_|_|/.
%   (N)   |_|_|_|/
%         |_|_|_|/
%          .
%          .
%          .
%          shape
%          funs
%           no (M)


% collNodes - [N x 4] array, each row stores [x,y, nx,ny] - global
%             Cartesian coordinates (x,y) of collocation point and the components 
%             [nx, ny] of unit normal vector at (x,y)

% It should be noted that though integrations are performed at one shot for
% all the collocation nodes (for all source points), the integrals
% computed for collocation nodes located at LocEdge element are incorrect. 
% At LocEdge element integrals become singular and they need a special
% treatment. That is why after "regular integration" performed for all
% collocation nodes, entries (rows) of H,G,R matrices corresponding to the
% collocation nodes at the LocEdge need to be recalculated. In fact, taking
% into account that we use only linear (geometric) elements and solve 
% elliptic problems, H matrix entries do not need recalculation (see ref. [1]): 
% Hi(i,i) = 0.5, and 
% Hi(i,j) = 0 for all j~=i.
% For computational efficiency reasons, the former formula is not implemented 
% here - inside GHR_Matrix_i() but in Gen_Matrices_dBEM() function, 
% on global H matrix (line 114 of this file).
% Entries of G and R that corresespond to singular integrals 
% (sources located at LocEdge element) are 
% recalculated with straightforward interval splitting approach:
% int_a^b f(x) dx = int_{a}^{x0} f(x) dx + int_{x0}^{b} f(x) dx
% where x0 is a singular point inside (a,b) interval. Each of the resulting
% integrals is evaluated with standard Q-point Gaussian quadrature.

%% Initialization 

    % choose appropriate weighting function depending on the type of
    % problem -> Eqs.(19-20) in ref. [3]    
    % fundamental solution and its derivative for Laplace/Poisson 2D problem
    if Helm_kappa == 0
        fh_G 	   = @(r) -log(r)./ (2*pi)  ; 
        fh_dG_dr   = @(r) -1 ./ (2*pi*r) ;          
    else
        % fundamental solution and its derivative for Helmholtz 2D problem
        kap = Helm_kappa ;

        fh_G 	 = @(r)  besselk(0, 1i*kap*r)/ (2*pi) ;
        fh_dG_dr = @(r) -(1i*kap/(2*pi)) * ( besselk(1, 1i*kap*r) )  ;        
    end
    
    % no of interpolation/collocation points
    % [N,~] = size(collNodes) ;
    % no of interpolation nodes/shape functions on element
     M = LocEdge.order +1 ;  
    % no of Gauss interpolation nodes
     Q = length(abscissa) ;

    % final size of structures used in integration is [N x M x Q]      
     
%% Interpolation nodes and shape functions
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Generating local coordinates of interpolation nodes and respective 
    % shape functions on element: 
    % interpolation_nodes_distribution = 0 if (LocEdge.order == 0) % constant element
    % interpolation_nodes_distribution is a vector of equidistant points in 
             % the interval [-1+offset,1-offset] for  higher order elements
    % m = 0:LocEdge.order;
    interpolation_nodes_distribution = (LocEdge.order~=0)*...
        linspace(-1+offset,1-offset, LocEdge.order+1) ;            
    
    % Getting global coordinates of interpolation nodes on the element      
    XintNodes = LocEdge.parametric(1) + 0.5 * (interpolation_nodes_distribution + 1) * LocEdge.parametric(3);  
    YintNodes = LocEdge.parametric(2) + 0.5 * (interpolation_nodes_distribution + 1) * LocEdge.parametric(4); 
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %%%% Computing shape functions
    % If we assume that all boundary elements are discretized in the same way
    % the code can be optimized by moving this part before "Sweeping the edges" loop.

    [S] = shapeFun1D(abscissa, interpolation_nodes_distribution ) ; % NOTE: line can be moved outside this function and passed as parameter
    % reshaping shape functions to match proper size of 3D grid constructed
    % for integration tructed for integration 
    S3DVec = reshape(S, 1, M, Q ) ;


    %% Generating the geometric data
    % Computing the length of the current edge
    L = sqrt((LocEdge.parametric(3))^2 + (LocEdge.parametric(4))^2); % NOTE: this can be calculated in preProcessing function and passed here as parameter
    % reshaping abscissa vector to match proper size of 3D itegration grid
    A3DVec = reshape(abscissa, 1,1, Q) ; 

    % Getting global coordinates and components of normal vectors for all Gauss points
    XGauss3DVec = LocEdge.parametric(1)  + 0.5 * (A3DVec + 1) * LocEdge.parametric(3);  
    YGauss3DVec = LocEdge.parametric(2)  + 0.5 * (A3DVec + 1) * LocEdge.parametric(4);
    
    % Computing the distances between Gauss points and collocation points    
        X3DVec = bsxfun(@minus, XGauss3DVec, collNodes(:,1) ) ;
        Y3DVec = bsxfun(@minus, YGauss3DVec, collNodes(:,2) ) ;
        R3DVec = hypot(X3DVec, Y3DVec) ;      
        % X3DVec,Y3DVec,R3DVec are [N x 1 x Q] arrays 
    
%% Computing the values of weighting functions for all integration points
    % weighting functions for G matrix
    G = fh_G(R3DVec) ; 
    
    % weighting functions for H matrix
    
        % Computing the components of the outward normal in Cartesian
        % directions (H matrix) for quadrature nodes
        nx = LocEdge.parametric(4) / L;
        ny = -1* LocEdge.parametric(3) / L;
        
        dG_dr = fh_dG_dr(R3DVec) ;
        dr_dn = (X3DVec * nx + Y3DVec * ny) ./ R3DVec ;
        
    dG_dn = dG_dr .* dr_dn ;        

%% Performing the side integration and updating the field values

    % Preparing weights for integration 
    w3DVec(1,1,:) = weight;

    % constructing integrands
    if LocEdge.order > 0         
        IntegrandG = bsxfun(@times, G   ,S3DVec) ; 
        IntegrandH = bsxfun(@times,dG_dn,S3DVec) ;
    else % there's no sense in multiplying by 1
        IntegrandG = G ;
        IntegrandH = dG_dn ;
    end
    
    Gi = L/2 * sum(bsxfun(@times,IntegrandG,w3DVec),3);
    Hi = L/2 * sum(bsxfun(@times,IntegrandH,w3DVec),3);

    if nargout > 2
        % this is an element with Robin BC -> 
        % there is a need to calculate local component of R vector
        
        % obtaining the equally spaced points on [-1,1] interval where
        % g function values are defined and stored in BConds.Robin
        a = linspace(-1,1,length( BConds.Robin{LocEdge.id,1} ));

        % obtaining the polynomial that interpolates the values in BConds.Robin
        if (isnan( BConds.Robin{LocEdge.id,1} ))
            error('local:consistencyChk',...
                'No Robin boundary conditions are defined on edge %d. \n',...
                LocEdge.id);
        else
            pol = polyfit(a, BConds.Robin{LocEdge.id,1},...
                length( BConds.Robin{LocEdge.id,1} )-1);
        end

        % computing the values of the interpolation polynomials at
        % quadrature nodes
        g = polyval(pol,abscissa);
        g = reshape(g,1,1,Q) ; % non-conjugate transpose

        IntegrandR = bsxfun(@times,g,G) ; % G .* repmat(g,N,1) ;
        Ri = L/2 * sum(bsxfun(@times,IntegrandR,w3DVec), 3) ;
        
        % vargout = Ri ; -> this is done later, after calculating singular integral
    end
    
    
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Special treatment of the singular functions - performing integration
%   For the collocation points which are placed on the boundary element 
%   the integrands are singular. Therefore, there is a need to redo the above
%   calculations but with modified abscissae. The modification should take
%   into consideration points of singularity on element LocEdge 
%   (vector: xi_singular stores local element coordinates of those points). 
%   xi_singular points correspond to singular
%   weighting function on the element LocEdge. Thus we generate two abscissae
%   for each singular node (one abscissa corresponds to quadrature nodes on 
%   left subinterval [a, x0], the other are quadrature nodes corresponding 
%   to the right subinterval of LocEdge [x0,b]). 
%   Integral evaluations are again vectorized and conducted in one-shot
%   with the use of specially prepared structures 

%              . . . . .
%             . . . . .
%            /_/_/_/|.    Q2 = total number of quad nodes on LocEdge = 2*Q
%           /_/_/_/|/        = quadrature nodes no on right subinterval of LocEdge (Q)
%          /_/_/_/|/          + quadrature nodes no on left subinterval of LocEdge (Q)
% internal/_/_/_/|/    
%  coll   |_|_|_|/  .
%  nodes  |_|_|_|/ . 
%  no     |_|_|_|/.
%  on     |_|_|_|/                          %%%%%%%%%%%%%%%%%%%%%%%%%%%
%  LocEdge|_|_|_|/                          % NOTES:
%  element .                                % N2 = Edges order = no of interpolation nodes per 
%          .                                %     boundary element (assuming discontinuous elements)
%    (N2)  .                                % M2 = M = N2
%          .                                % Q2 = 2*Q
%          shape                            %
%          funs                             %
%           no (M2)



    % Finding indices of rows of these nodes in collNodes array
    % If there was an assumption that nodes in collNodes are sorted in
    % consecutive order there wouldn't be need for finding them. They
    % would be then in [LocEdge.insert:LocEdge.insert+LocEdge.dim-1] rows.
    
        % Detect relevant points of singularity on LocEdge and store their 
        % local abscissae in xi_singular vector.
        % Only internal nodes are trouble-makers. Nodes that coincide with 
        % geometry nodes shouldn't do harm so we can just omit them:
        if (LocEdge.order > 0 && offset == 0 )
            % In case of having continuous elements in the future
            % we need to winnow out the collocation nodes that
            % coincide with geometry nodes.
            
            xi_singular = interpolation_nodes_distribution(2:end-1) ;
            X_singular = XintNodes(2:end-1) ;
            Y_singular = YintNodes(2:end-1) ;
        else
            % if only internal nodes exist
            xi_singular = interpolation_nodes_distribution;
            X_singular = XintNodes ;
            Y_singular = YintNodes ;
        end
            
	% Find the indices of rows in collNodes array that correspond to
	% singular points and store them in rowInd (Tolerance of checking  
    % procedure is related to domain's characteristic length)
    tol = dChL * 10^-6 ; 
    rowInd = find( sum(abs( bsxfun(@minus,collNodes(:,1), X_singular) ) < tol & abs( bsxfun(@minus,collNodes(:,2), Y_singular) ) < tol, 2) ) ;

    % Integrals stored in rows [rowind] of H,G,R matrices were miscalculated
    % Zero them out.
    Gi(rowInd,:) = zeros(size(Gi(rowInd,:))) ;
    % H elements are zeros and they should be zeros except the H(i,i)
    % elements which are updated at once outside this function
    Hi(rowInd,:) = zeros(size(Hi(rowInd,:))) ; % this line can be commented out    
    if nargout > 2
        Ri(rowInd,:) = zeros(size(Ri(rowInd,:))) ;
    end
    
    
        
    % Replacing the singular integral with the sum of two integrals
    % having the singularity point at the end of the interval. 

    % constructing new abscissa (transforming both subintervals onto 
    % [-1 1] and merge them together)
    newAbscissaA = intervalTransformation(abscissa, -1, xi_singular) ;
    newAbscissaB = intervalTransformation(abscissa,  xi_singular, 1) ;   
    abscissa2 = [newAbscissaA; newAbscissaB ] ;
    
    % computing Jacobian for both transformations above
    Ja  = (xi_singular+1) .* 0.5 ; 
    Jb  = (1-xi_singular) .* 0.5 ;
    % calculating quadrature weights for abscissa2
    weights2 = [bsxfun(@times,weight,Ja); bsxfun(@times,weight,Jb) ];

    % no of interpolation nodes/ shape functions (on element)
    N2 = length(xi_singular) ; 
    
%% Interpolation nodes and shape functions

    interpolation_nodes_distribution2 = xi_singular ;        

    % calculating shape functions for abscissa2
    [S2] = shapeFun1D(abscissa2, interpolation_nodes_distribution2 ) ;
    % permuting shape functions to match proper size of 3D  grid constructed for integration 
    S2_3DVec = permute(S2, [3 1 2] ) ;

    % L stays the same as before
    % reshaping abscissa vector to match proper size of 3D itegration grid
    A2_3DVec = permute(abscissa2, [2 3 1] ) ; 
    
    % Getting global coordinates and components of normal vectors for all Gauss points
    X2_Gauss3DVec = LocEdge.parametric(1)  + 0.5 * (A2_3DVec + 1) * LocEdge.parametric(3) ;
    Y2_Gauss3DVec = LocEdge.parametric(2)  + 0.5 * (A2_3DVec + 1) * LocEdge.parametric(4) ;

    % Computing the distances between Gauss points and collocation points 
    X2_3DVec = bsxfun(@minus, X2_Gauss3DVec, X_singular' ) ;
    Y2_3DVec = bsxfun(@minus, Y2_Gauss3DVec, Y_singular' ) ;
    R2_3DVec = hypot(X2_3DVec, Y2_3DVec) ;  

    % Computing the values of weighting functions for all singular points
    % on the element
    G2 = fh_G(R2_3DVec) ;

    % reshaping weight vector to match proper size of 3D itegration grid
    w2_3DVec = permute(weights2, [2 3 1] ) ; 
    w2_3D = repmat(w2_3DVec, [1,N2,1]) ;

    IntegrandG2 = L/2 * bsxfun(@times, G2   ,S2_3DVec ) ;

    temp = IntegrandG2 .* w2_3D ;
    Gi (rowInd,:) = Gi(rowInd,:) + sum(temp,3) ;

    if nargout > 2
        % this is an element with Robin BC -> 
        % there is a need to calculate local component of R vector

        % computing the values of the interpolation polynomials of 
        % g function at quadrature nodes
        % pol is defined earlier - we just use it here
        g2 = polyval(pol,A2_3DVec) ;            

        IntegrandR2 = g2.*G2 ; 
        Ri(rowInd,:) = L/2 * sum(bsxfun(@times,IntegrandR2,w2_3DVec), 3) ;

        vargout = Ri ;

    end
    
end % function Gen_Matrices_dBEM(...)

function Vy = intervalTransformation(Vx, c, d) 

% INTERVALTRANSFORMATION transforms points RVx lying in normalized [-1, 1] 
% interval onto [c, d] interval.
% Function is vectorized - RVx should a vector of points in [-1,1], 
% c and d can be arbitrarily long vectors of intervals starting points and
% endpoints, respectively.

% Input data: 
% Vx - vector of values from [-1 1] interval
% c,d - vectors storing beginnings and endpoints of corresponding intervals

% Output:
% Vy - each k-th column of the output is a vector Vx(:) after
%           transformation onto [c(k) d(k)] interval

% EXAMPLE OF USAGE:
% Transform points [-1 0 1] onto intervals [0,3], and [10, 20]:

% >> intervalTransformation([-1 0 1],  [0 10], [3 20]) 
% ans =
%          0   10.0000
%     1.5000   15.0000
%     3.0000   20.0000

    a = -1 ;
    b =  1 ;
    c = (c(:))' ;
    d = (d(:))' ;
    z1 = (d-c) / (b-a) ;
    z2 = c - z1 * a ;

    % Vy = bsxfun(@times, Vx, z1) + z2 ;
    Vy = bsxfun(@plus, bsxfun(@times, Vx(:), z1), z2 ) ;
    
end % function RVy = intervalTransformation(...) 
