function varargout = dBMT_BC1(varargin)
% DBMT_BC1 MATLAB code for dBMT_BC1.fig
%      DBMT_BC1, by itself, creates a new DBMT_BC1 or raises the existing
%      singleton*.
%
%      H = DBMT_BC1 returns the handle to a new DBMT_BC1 or the handle to
%      the existing singleton*.
%
%      DBMT_BC1('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in DBMT_BC1.M with the given input arguments.
%
%      DBMT_BC1('Property','Value',...) creates a new DBMT_BC1 or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before dBMT_BC1_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to dBMT_BC1_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help dBMT_BC1

% Last Modified by GUIDE v2.5 20-Nov-2019 12:24:24

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @dBMT_BC1_OpeningFcn, ...
                   'gui_OutputFcn',  @dBMT_BC1_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT

% UIWAIT makes dBMT_BC1 wait for user response (see UIRESUME)
% uiwait(handles.figure1);

% --- Outputs from this function are returned to the command line.
function varargout = dBMT_BC1_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;



%% OPENING FUNCTION
% * Executes just before |dBMT_BC1| is made visible;
% * Reads the mesh data from the |mat| file of the previous GUI and
% constructs the topological matrices;
% * If the |mat| file associated to the current GUI exists (meaning that
% the previous GUI was not changed), it loads the boundary information,
% otherwise it sets all boundaries to Dirichlet.
function dBMT_BC1_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to dBMT_BC1 (see VARARGIN)

%%
% Creates the |handles| structure
% Choose default command line output for |dBMT_BC1|
handles.output = hObject;

%%
% Loading the mesh information.
load('dBMT_StructDef','UI','p','e', 't');

%% I MERGED THE FOLLOWING SECTOR INTO CreateBEMesh 

% ********************* START ***************************
% for non-regular meshes, reordering the edges to have the domain on their
% left and removing the interior edges caused by the union of domains in
% the mesh generator
% if ~(isscalar(e) && e == 0) % if e is a null scalar, the mesh is regular
% 
%     % check whether edges structure 'e' contains any interior edges
%     % (it may happen if domain is built as a union of more than one
%     % simplices)
%     % if it is true -> remove columns corresponding to them
%     mask = ( (e(6,:) ~= 0) & (e(7,:) ~= 0) ) ;
%     e(:,mask) = [] ;
%     
%     
%     % Reshaping e to be consistent with the desired orientation
%     mask=(e(6,:)==0); % detects edges with the wrong orientation (having an element on the right)
%     e([1 2 6 7],mask)=e([2 1 7 6],mask); % switches to the right orientation
% 
%     % analyse the geometry of the mesh and detects the polygons that form
%     % it
%     [e, ePolygon, polygonNo] = DetectPolygonsInPETMesh(e) ;
%     
%     % updating the dBMT_StructDef.mat file with the edges structure
%     % correctly reshaped
%     save('dBMT_StructDef','-append','e') ;    
%     
%     % SOME COMMENT FROM MARIUSZ
%     save('irreg_mesh', 'e', 'ePolygon','polygonNo') ;
% end
% ************************* FINISH *****************************


%% CREATING THE GEOMETRY
% The geometry creation in FreeHyTE has at its core the division of the
% domain into geometrically simpler subdomains. This enables the unified
% treatment of the Finite Element and Boundary Element Methods and the use
% of the mesh generator built into Matlab. The subdomain mesh generation is
% performed using CREATEREGMESH (for regular, rectangular domains), and
% CREATEBEMESH (for non-regular domains meshed using the pdetool mesh
% generator). After the creation of the mesh, the boundary elements are
% extracted as the boundaries of the mesh connected to a single subdomain.
% Generate mesh information
if UI.MeshOption == 1 
    % Generate subdomain mesh information for regular, rectangular
    % meshes. 
    % * INPUT: domain's dimensions and number of finite elements in
    % the Cartesian directions;
    % * OUTPUT: 
    %   + nodes: list with the nodes' Cartesian coordinates;
    %   + edges_nodes: list with the indices of the nodes at the beginning
    %   and end of an edge;
    %   + edges_loops: list with the subdomains on the left and right of
    %   each edge.
    [nodes, edges_nodes, ~, ~] = CreateRegMesh(UI.L,UI.B,UI.Nx,UI.Ny);   
else               % Irregular mesh
    % Generate subdomain mesh information for non-regular meshes, created
    % using PDETOOL. The input are the exterior edges (e) and nodes (p)
    % information produced by PDETOOL. The outputs are the same as
    % described above.
            
    [nodes,edges_nodes] = CreateBEMesh(p,e);

    if UI.ProblemType == 2 % if external problem

        % For external problem irreg_mesh.mat was created in CreateBEMesh() 
        % from line 146. Now we need to use it in order to remove all the 
        % external elements.
        
        load('irreg_mesh', 'ePolygon','polygonNo', 'index_of_external_polygon') ;

        if polygonNo > 1
            %2DEL error('In this problem number of polygons in mesh should be >=2 !')
            
            % preparing list of edges of internal polygons in case of external problem
            internal_edges_nodes = edges_nodes((ePolygon(1,:) ~= index_of_external_polygon),:) ;
            edges_nodes = internal_edges_nodes ;
        end
        
        
    end
    
end

% edgeArray is simply a list of boundary elements, listed such as to
% circumvent the domain in a counter-clockwise direction (eventual inner
% holes are circumvented in a clockwise direction).
edgesArray = 1:length(edges_nodes(:,1));

%%
% Publishing the topological information in the |handles| structure, to
% make it available to all functions associated to the current GUI.
set(handles.listbox1,'string',edgesArray);
handles.edgesType = get(handles.popupmenu1,'String');
handles.data = cell(length(edgesArray),2);

%%
% If there exists a local |mat| file, it loads its key elements to fill in
% the fields with data taken from the previous run...
if exist('./dBMT_BC1.mat','file') % reuse the previous data
    load('dBMT_BC1','data');
    handles.data=data;
    %%
    % ... otherwise it just presets all boundaries to 'Dirichlet' type.
else
    for i=1:length(edgesArray)
        handles.data{i,1}=edgesArray(i);
        handles.data{i,2}=handles.edgesType{1};  % Dirichlet as predefined boundary type
    end
end

%%
% Creates the table where the boundaries are listed, along with their types
column = {'Boundary ID','Boundary Type'};
uitable('units','Normalized','Position',[0.75, 0.22, 0.13, 0.35],'Data',handles.data,...
'ColumnName',column,'RowName',[]);

%% 
%storing all structures
handles.edgesArray = edgesArray;
handles.nodes = nodes;
handles.edges_nodes = edges_nodes;

%%
% Generates the code for drawing the mesh, along with the mesh information
% buttons

% getting the geometrical limits of the structure
limxmin = min(nodes(:,1));
limxmax = max(nodes(:,1));
limymin =  min(nodes(:,2));
limymax =  max(nodes(:,2));
nedge = length(edgesArray);
nnode = length(nodes(:,1));

% Plotting the Boundary Element Mesh
% Initialization of the required matrices
X = zeros(2,nedge) ;
Y = zeros(2,nedge) ;
% Extract X,Y coordinates for the (iel)-th element
for iel = 1:nedge
    X(:,iel) = nodes(edges_nodes(iel,:),1) ;
    Y(:,iel) = nodes(edges_nodes(iel,:),2) ;
end

% drawing the mesh
patch(X,Y,'w'); hold on; plot(X(:),Y(:),'o','MarkerEdgeColor','k',...
    'MarkerFaceColor','k','MarkerSize',15); hold off;
axis([limxmin-0.01*abs(limxmin) limxmax+0.01*abs(limxmax) limymin-0.01*abs(limymin) limymax+0.01*abs(limymax)]);
axis equal;
axis off ;


% To display Node Numbers 
axpos = getpixelposition(handles.axes2); % position & dimension of the axes object
% Define button's weight and height
bweight = 60;
bheight = 20;
pos = [((axpos(1)+axpos(3))/2) (axpos(2)-2.5*bheight) bweight bheight]; % align the second button with the center of the axes obj limit
ShowNodes = uicontrol('style','toggle','string','Nodes',....
    'position',[(pos(1)-2*bweight) pos(2) pos(3) pos(4)],'background',...
    'white','units','Normalized');

% To display Boundary Numbers
ShowEdges = uicontrol('style','toggle','string','Edges',....
    'position',[(pos(1)+2*bweight) pos(2) pos(3) pos(4)],'background','white',...
    'units','Normalized');

set(ShowNodes,'callback',...
    {@SHOWNODES,ShowEdges,nodes,edges_nodes,X,Y,nnode,nedge});
set(ShowEdges,'callback',...
    {@SHOWEDGES,ShowNodes,nodes,edges_nodes,X,Y,nnode,nedge});

%%
% Updates the handles structure
guidata(hObject, handles);


%% NEXT BUTTON
% * Executes on button press in |pushbutton_next|;
% * It recovers all data provided by the user in the GUI fields and the
% topological matrices computed in the opening function;
% * It checks for relevant (i.e. mesh and boundary type) changes as 
% compared to the previous |mat| file. If such changes exist, it deletes 
% the |mat| file corresponding to the next GUI to force its definition from 
% scratch. If no mesh changes were detected, the next GUI will load its 
% previous version;
% * If the user asked for the model to be saved, it saves the information
% regarding this GUI in the specified file and folder.

function pushbutton_next_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton_next (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

%%
% Recovering the data created or set by the user in the current GUI
data=handles.data;
edgesType=handles.edgesType;
edgesArray = handles.edgesArray;
nodes = handles.nodes;
edges_nodes = handles.edges_nodes;

% check if critical changes were made in the current GUI session
if exist('./dBMT_BC1.mat','file')
    save('ToDetectChanges','data'); % temporary file to detect critical changes
    % Fields whose change triggers the change of the mesh
    CriticalFields = {'data'};
    % Checks the old dBMT_BC1 and the new ToDetectChanges for changes in
    % the critical fields. ChangesQ = 1 if there are changes, 0 otherwise
    ChangesQ = any(~isfield(comp_struct(load('dBMT_BC1.mat'),...
        load('ToDetectChanges.mat')),CriticalFields));
    % deleting the auxiliary file
    delete('ToDetectChanges.mat');
    %%
    % If the |mat| file associated to the current GUI session does not exist,
    % it is likely because the previous GUI was modified. In this case the
    % next GUI needs to be reinitialized.
else
    ChangesQ = 1;
end

%%
% Saving the workspace to the local |mat| file. If requested by the
% user, the GUI information is also _appended_ to the file specified by the
% user.
load('dBMT_StructDef','DirName','FileName');
% if requested by the user, saves to file
if ~isempty(DirName)
    save(fullfile(DirName,FileName),'-append',...
        'data','edgesType','nodes','edges_nodes','edgesArray');
end
% saves the local data file
save('dBMT_BC1',...
    'data','edgesType','nodes','edges_nodes','edgesArray');

% saves the data required by Visualize to the local data file
save('Visualize','nodes','edges_nodes');

%%
% Preparing to exit.
close(handles.figure1);

% Trying to close Visualize if it was opened
try
    close('Visualize');
catch
end

%%
% If there are relevant changes, it physically deletes the |mat| files
% associated to the next GUI.
if ChangesQ
    delete('dBMT_BC2.mat');
end

dBMT_BC2;



%% RESET BUTTON
% * Executes on button press in |pushbutton_reset|.
% * It resets all tables and defines all boundaries as Dirichlet.

function pushbutton_reset_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton_reset (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

%%
% Setting all edges to Dirichlet type...
edgesArray = handles.edgesArray;
handles.edgesType = get(handles.popupmenu1,'String');
handles.data = cell(length(edgesArray),2);
for i=1:length(edgesArray)
    handles.data{i,1}=edgesArray(i); 
    handles.data{i,2}=handles.edgesType{1};  % Dirichlet as predefined boundary type
end

%%
% ... and redrawing the table.
column = {'Boundary ID','Boundary Type'};
uitable('units','Normalized','Position',[0.75, 0.22, 0.13, 0.35],...
    'Data',handles.data,'ColumnName',column,'RowName',[]);

%%
% Updates the handles structure
guidata(hObject,handles);


%% PREVIOUS BUTTON
% * Executes on button press in |pushbutton_previous|;
% * Just closes the current GUI and launches the previous one. All changes
% made in the current GUI are loUI.
function pushbutton_previous_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton_previous (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
close(dBMT_BC1);

% Trying to close Visualize if it was opened
try
    close('Visualize');
catch
end

dBMT_StructDef(2);


%% ASSIGN BUTTON
% * Executes on button press in |pushbutton_assign|;
% * Fills in the boundary type table for the boundaries selected in
% |listbox1| with 'Dirichlet' or 'Neumann', as defined in |popupmenu1|.

function pushbutton_assign_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton_assign (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

%%
% Gets the selected items in |listbox1|
itemlist = get(handles.listbox1,'value'); % list of selected items in the listbox
nitems = length(itemlist); % number of selected items in the listbox

%%
% Sets the |data| for the selected items to the type selected in
% |popupmenu1|.
if get(handles.popupmenu1,'value')==1   % if Dirichlet is selected
    for ii = 1:nitems
        crtitem = itemlist(ii);
        handles.data{crtitem,2}=handles.edgesType{1};
    end
elseif get(handles.popupmenu1,'value')==2   % if Neumann is selected
    for ii = 1:nitems
        crtitem = itemlist(ii);
        handles.data{crtitem,2}=handles.edgesType{2};
    end
else         % if Robin is selected
    for ii = 1:nitems
        crtitem = itemlist(ii);
        handles.data{crtitem,2}=handles.edgesType{3};
    end
end

%%
% Updates the handles structure
guidata(hObject,handles);

%% 
% Redraws the table
column = {'Boundary ID','Boundary Type'};
uitable('units','Normalized','Position',[0.75, 0.22, 0.13, 0.35],...
    'Data',handles.data,'ColumnName',column,'RowName',[]);



%% ENLARGE BUTTON
% * Executes on button press in |pushbutton_enlarge|;
% * Launches the |Visualize| visualizer.

function pushbutton_enlarge_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton_enlarge (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


%%
% Recovering the data created or set by the user in the current GUI
nodes = handles.nodes;
edges_nodes = handles.edges_nodes;

% saves the data required by Visualize to the local data file
save('Visualize','nodes','edges_nodes');

Visualize;



%% GUI ENTITIES GENERATION CODE
% * Automatically generated code

% --- Executes on selection change in popupmenu1.
function popupmenu1_Callback(hObject, eventdata, handles)
% hObject    handle to popupmenu1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns popupmenu1 contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popupmenu1


% --- Executes during object creation, after setting all properties.
function popupmenu1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popupmenu1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in listbox1.
function listbox1_Callback(hObject, eventdata, handles)
% hObject    handle to listbox1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns listbox1 contents as cell array
%        contents{get(hObject,'Value')} returns selected item from listbox1


% --- Executes during object creation, after setting all properties.
function listbox1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to listbox1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: listbox controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
