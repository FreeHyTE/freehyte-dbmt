function [eSorted, eAuxSorted, polygonNo, index_of_external_polygon] ...
                                       = DetectPolygonsInPETMesh(e, p)

% DETECTPOLYGONSINPETMESH - (for irregular meshes) detects and sorts 
%           boundary elements such as they are indexed properly (i.e. the
%           edges of the same polygon as a consecutive groups of indices,
%           edge no. 1 starts from one of the internal polygons,
%           external edges are the last in the list

% Input data: 
% e - edges structure after obtaining it from Matlab's PDE Toolbox and
%     premodifying it in dBMT_BC1()
% p - nodes of the geometry

% Output data:
% eSorted       - e structure sorted in a right way
% eAuxSorted    - auxiliary structure that stores number of polygon as a first 
    % row and the numbers of consecutive elements of the boundary    
% polygonNo     - no. of polygons
% index_of_external_polygon - index of outermost polygon

% BIBLIOGRAPHY


% declaration of auxiliary variables
edgesNo = size(e,2) ;
counter = 1 ;         % counts consecutive boundary elements 
ind = 1 ;             
polygonNo = 1 ;       % no. of current polygon(during the computations), 
                    % and the total number of polygons (when the
                    % compuatations are over)
StartingNodeOfCurrentPolygon = e(1,1) ;

eAux = zeros(2, edgesNo) ;     % auxiliary structure that stores the number of polygon 
                        % to which the edge belong to, and the (new, consecutive) number of
                        % edge itself

while counter <= edgesNo % while there are some boundary elements left to consider...

    Node1 = e(1, ind) ;  % store indices of the beginning and ..
    Node2 = e(2, ind) ;  % the end node of the element ind 

    % mark this edge of the polygon
    eAux(1,ind) = polygonNo ; 
    eAux(2,ind) = counter ;   

    if Node2 ~= StartingNodeOfCurrentPolygon % if we are still on the same polygon            
        % find next consecutive edge of current polygon
        ind = find( e(1,:) == Node2 ) ;                        
    else % if we made the whole loop and reached the node we started walking along polygon
        polygonNo = polygonNo + 1 ;
        indicesOfEdgesNotVisitedYet = find((eAux(1,:) == 0)) ; 
        if ~isempty(indicesOfEdgesNotVisitedYet)
           ind =  indicesOfEdgesNotVisitedYet(1) ;
        end
        StartingNodeOfCurrentPolygon = e(1,ind) ;
    end

    counter = counter + 1  ;
end
polygonNo = polygonNo - 1 ; % decrease the number because last increasing was invalid
counter = counter - 1; 


% mark this edge of the polygon   
eAux(1,ind) = polygonNo ;
eAux(2,ind) = counter ;   
%%%%%%%%%% 

[B,I] = sort( eAux(2,:)) ; 
eSorted = e(:,I) ;         
eAuxSorted = eAux(:,I) ;

%%%%%%%%%% fixing the bug with the first element 'before' the corner
if (eSorted(3,1) == 0) % fixing the fix -> swapping between the first and
        % the last element of external polygon should be performed only
        % if the first element has its starting poing located at 0 in local
        % coordinates of the polygon edge i.e. eSorted(3,1) == 0.
        % otherwise we assume, that swapping has been already conducted.

    if polygonNo == 1
        indexOfLastEdgeOfExternalPolygon = size(eAux,2) ;
    else
        % indexOfLastEdgeOfExternalPolygon = min(find(eAux(1,:) == 2)) - 1 ;
        % compute how many elements make up an external polygon (polygon numbered with 1)
        indexOfLastEdgeOfExternalPolygon = sum(eAux(1,:) == 1) ;
    end

    % swap the first and last edge of the first polygon
    eSorted(:, [1:indexOfLastEdgeOfExternalPolygon]) = ...
            eSorted(:, [2:indexOfLastEdgeOfExternalPolygon 1]) ;
 
end
%%%%%%%%%% 


%% Make sure that polygons are correctly ordered
% i.e. the edges that belong to the external boundary are listed at the end
% of the structures

% Since e passed to DetectPolygonsInPETMesh(e) can already be after
% reordering, we cannot assume that external boundary is listed as the
% first one. We need to check it explicitly.

% find the index of the outermost geometry node:
dist = sqrt(p(1,:).^2 + p(2,:).^2) ;
[~,ind_outermost]=max(dist) ;

% find the edge (boundary element) which consist of the outermost node (it
% belongs to the external polygon)
ind_of_edge_of_external_polygon =  ( find(eSorted(1,:) == ind_outermost) ) ;


% check if this edge belongs to the polygon no 1 ...
index_of_external_polygon = eAuxSorted(1, ind_of_edge_of_external_polygon) ;



% % ENDOF DetectPolygonsInPETMesh()
