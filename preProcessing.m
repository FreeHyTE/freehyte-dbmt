function [preProcStruc] = preProcessing(Nodes, Edges, offset,MeshOption)  

% PREPROCESSING conducts some auxiliary calculations required on later
% stages of the programme. PREPROCESSING returns preProcStruc  
% (preProcessingStructure) - structure that stores all the needed information.

% Input data: 
% Edges and BConds -- structures that describe the problem
% offset -- the offset of the interpolation nodes of the boundary
%           elements from their geometric limits
% MeshOption -- MeshOption == 1 - regular mesh
%               MeshOption == 2 - irregular mesh

% Output data:
% preProcStruc.collNodes - information about collocation nodes. 
         % each row of collNodesArr matrix stores [x,y,nx,ny] values
         % which are:
         % x,y - global cartesian coordinates of collocation point
         % nx,ny - unit normal vector in collocation point (needed in dMFS) 

% preProcStruc.dChL     - domain characteristic length ([1],[2]), the
%                   longest chord of the domain (needed in dTHM)

% preProcStruc.TreffC  - center of Trefftz eigenexpansion 
%                       calculated as barycenter of the domain (needed in dTHM).

% BIBLIOGRAPHY
% 1. C.-S. Liu. An effectively modified direct Trefftz method for 
%     2D potential problems considering the domain's characteristic length. 
%     Engineering Analysis with Boundary Elements, 31(12):983-993, Dec. 2007
% 2. Borkowski M, Moldovan ID - Direct Boundary Method Toolbox for some
%    elliptic PDEs in FreeHyTE framework, submitted to Advances in 
%    Engineering Software, 2019

                                                                            

% no of collocation nodes calculated with the assumption that the
% interpolation degree is the same on all boundary elements ->
% no_of_boundary_elements * no_of_interpolation_nodes_per_edge
    % If we allow user to have different order of boundary interpolation on
    % different elements then this part should be changed.
noOfCollNodes = length(Edges.nini) * (Edges.order(1)+1) ;

% [dChL,TreCenter] = computeDomainCharacteristicLengthAndTrefftzCenter(Nodes) ;

if MeshOption == 1 % if regular mesh
    [dChL,TreCenter] = TrefftzParamsForRegularMesh( Nodes) ;
else % irregular mesh
    load('irreg_mesh','polygonNo') ;
    
    if polygonNo == 1  % Although mesh was created in pdetool, it still 
            % consists of only one boundary and it can be considered as a
            % regular one.
        [dChL,TreCenter] = TrefftzParamsForRegularMesh( Nodes) ;
    else    
        [dChL,TreCenter] = TrefftzParamsForIrregularMesh( Nodes, Edges ) ;
    end
end 

% initializing array which stores information about collocation nodes
collNodesArr = zeros(noOfCollNodes,4) ;

    % updating collNodesArr with collocation nodes from consecutive
    % boundary elements
    for ii=1:length(Edges.nini) 

            LocEdge =  struct('id',ii,'parametric',Edges.parametric(ii,:),...
                'order',Edges.order(ii),'insert',Edges.insert(ii),...
                'dim',Edges.dim(ii));

            % local coordinates of interpolation nodes
            interpolation_nodes_distribution = ...
                (LocEdge.order~=0)*linspace(-1+offset,1-offset, LocEdge.order+1) ;   

            % global coordinates of interpolation nodes        
            X = LocEdge.parametric(1)  + 0.5 * (interpolation_nodes_distribution + 1) * LocEdge.parametric(3);  
            Y = LocEdge.parametric(2)  + 0.5 * (interpolation_nodes_distribution + 1) * LocEdge.parametric(4);        

            % Computing the components of the outward normal in the Cartesian directions
            L = sqrt((LocEdge.parametric(3))^2 + (LocEdge.parametric(4))^2) ; 
            nx = LocEdge.parametric(4) / L ;
            ny = -1* LocEdge.parametric(3) / L ;
            
            % update the values in rows of collNodesArr corresponding to
            % the collocation nodes located on ii-th element
            collNodesArr((ii-1)*(Edges.order(ii)+1)+1 : (ii)*(Edges.order(ii)+1),:) = [X',Y',nx*ones(size(X')),ny*ones(size(X'))] ;
                        
    end % for ii=1:length(Edges.nini)
   
preProcStruc = struct('collNodes', collNodesArr, 'dChL', dChL, 'TreffC', TreCenter) ;

end % ENDOF function preProcessing


function [dChL,r0] = TrefftzParamsForIrregularMesh(Nodes, Edges)
    
    load('irreg_mesh', 'e', 'ePolygon','polygonNo') ; % this works provided irreg_mesh stores geometry of the valid problem
    
    load('dBMT_StructDef', 'p', 'UI')
    p = p' ;  % we need to use p instead of Nodes in this function because 
            % Nodes contain shortened list of nodes and the indices in 
            % 'e' refer to p, not to Nodes
               
    DCHL = zeros(polygonNo,1) ;   % 1st entry refers to external polygon and is not used
    R0   = zeros(polygonNo, 2 ) ; % 1st entry refers to external polygon and is not used
                                  % We remove it at the end of this function
    
    for ii=1:polygonNo
        iithPolygonEdgesMASK = ePolygon(1,:)==ii ;
        
        % compute the distances between geometry Nodes of the ii-th polygon
        R = sqrt ( (bsxfun(@minus, p(e(1,iithPolygonEdgesMASK),1), p(e(1,iithPolygonEdgesMASK),1)' )).^2 ... % = (Nodes(:,1) - [Nodes(:,1)]').^2 == (x - x')^2
               +(bsxfun(@minus, p(e(1,iithPolygonEdgesMASK),2), p(e(1,iithPolygonEdgesMASK),2)' ).^2) ) ;
           
        DCHL(ii) = max(max(R)) ;   
        
        % compute the barycenter of the ii-th polygon
        % R0(ii,:) = mean( p( e(1,iithPolygonEdgesMASK),:) ) ;
        R0(ii,:) = polygonCentroid( p( e(1,iithPolygonEdgesMASK),1), p( e(1,iithPolygonEdgesMASK),2) ) ;
    end
    
    
    %% Determining dChL and r0 for exterior and interior problems for multipolygon problems
    
    % ways of computing final shift of the auxiliary nodes for all the
    % polygons:
    % 0) final shift = dChL_max * lambda - inherited from the original approach for interior problems (unreliable, auxiliary nodes can easily be shifted too far, to the domain of interest)
    % 1) final shift = dChL_min * lambda - the same distance for all the polygons (more reliable, only very thin cavities can be the cause of problems...)
    % 2) final shift = dChL_i * lambda   - different absolute distances for different polygons (more reliable, only very thin cavities can be the cause of problems...)
    % 3) final shift = dChL_i * lambda_i - probably too complicated
    
    % chosen: 1) 
    dChL = min(DCHL) ;
    
    
    % determine the index of the extarnal polygon - the one 
    % for which dChLis the largest
    [max_dchl, ind_external] = max(DCHL) ;
    
    if UI.ProblemType == 1 % interior
        % centre of eigenexpansion should be located in the centre 
        % of the external polygon               
        
        r0   = R0(ind_external, :) ;
    else % UI.ProblemType == 2 % exterior
        % as the final one and only r0 for all cavities choose the one that is
        % closest to the barycentre of the external polygon.                    %%%(0,0). 
        % Do not use the barycenter of the external polygon!
        
        r0_of_the_external = R0(ind_external,:) ;
        R0(ind_external,:) = [] ; % remove the barycenter of the external polygon from the list provided ...
                % there are more barycenters that were computed
        
        dist = ( (R0(:,1) - r0_of_the_external(1)) .^2 + ...
                  (R0(:,2) - r0_of_the_external(2)) .^2) ;
        ind = find(min(dist) == dist) ;
        r0 = R0(ind,:) ;
    end    

end % ENDOF function TrefftzParamsForIrregularMesh


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Auxiliary function that computes domain characteristic length (dChL) and 
% optimal position (r0) for center of Trefftz eigenexpansion

function [dChL,r0] = TrefftzParamsForRegularMesh(Nodes)
    
    %% compute the distances between geometry Nodes
    R = sqrt ( (bsxfun(@minus, Nodes(:,1), Nodes(:,1)' )).^2 ... % = (Nodes(:,1) - [Nodes(:,1)]').^2 == (x - x')^2
               +(bsxfun(@minus, Nodes(:,2), Nodes(:,2)' ).^2) ) ; % % (Nodes(:,2) - [Nodes(:,2)]').^2 == (y - y')^2
    
    % compute domain characteristic length - i.e. the longest chord of the domain
    dChL = max(max(R)) ;
    
    %% compute 'the best' place for Trefftz eigenexpansion center
     % version 2 - barycenter of the domain 
     % r0 = mean(Nodes) ;
     r0 = polygonCentroid(Nodes(:,1), Nodes(:,2) ) ;
end % ENDOF function  TrefftzParamsForRegularMesh
