function varargout = dBMT_BC2(varargin)
% DBMT_BC2 MATLAB code for dBMT_BC2.fig
%      DBMT_BC2, by itself, creates a new DBMT_BC2 or raises the existing
%      singleton*.
%
%      H = DBMT_BC2 returns the handle to a new DBMT_BC2 or the handle to
%      the existing singleton*.
%
%      DBMT_BC2('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in DBMT_BC2.M with the given input arguments.
%
%      DBMT_BC2('Property','Value',...) creates a new DBMT_BC2 or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before dBMT_BC2_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to dBMT_BC2_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to next (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help dBMT_BC2

% Last Modified by GUIDE v2.5 20-Nov-2019 12:25:37

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
    'gui_Singleton',  gui_Singleton, ...
    'gui_OpeningFcn', @dBMT_BC2_OpeningFcn, ...
    'gui_OutputFcn',  @dBMT_BC2_OutputFcn, ...
    'gui_LayoutFcn',  [] , ...
    'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% UIWAIT makes dBMT_BC2 wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = dBMT_BC2_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


%% OPENING FUNCTION
% * Executes just before |dBMT_BC2| is made visible;
% * Reads the mesh data from the |mat| file of the previous GUI and
% constructs the lists of Dirichlet and Neumann boundaries;
% * If the |mat| file of the current GUI exists (meaning that the 
% previous GUI was not changed), it loads the boundary information,
% otherwise it sets all boundaries to Dirichlet.

function dBMT_BC2_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to dBMT_BC2 (see VARARGIN)

%%
% Creates the |handles| structure
% Choose default command line output for |dBMT_BC2|
handles.output = hObject;

%%
% Loads the exterior edges and their types from the previous GUI
load('dBMT_BC1','edgesArray','data','edgesType');

%%
% Creates two arrays listing the exterior Dirichlet and Neumann edges
% (|edgesDirichlet| and |edgesNeumann|)
for i=1:length(edgesArray)
    if strcmp(data{i,2},edgesType{1}) % if the edge type is Dirichlet
        handles.edgesDirichlet(i,1)=edgesArray(i); % list with the Dirichlet edges
    elseif strcmp(data{i,2},edgesType{2}) % if the edge type is Neumann
        handles.edgesNeumann(i,1)=edgesArray(i);  % list with the Neumann edges
    else
        handles.edgesRobin(i,1)=edgesArray(i);  % list with the Robin edges
    end
end

%%
% *Dirichlet boundary conditions*

%%
% Generates the |dataDir| matrix to store the id of the exterior Dirichlet 
% edges and their enforced boundary conditions. The |dataDir| matrix is 
% created from from scratch or imported from the |dBMT_BC2| file, if 
% such file exists (meaning that the previous GUI was left unchanged).


if isfield(handles,'edgesDirichlet')==1
    handles.edgesDirichlet(handles.edgesDirichlet==0)=[];
    
    %%
    % Writes all |edgesDirichlet| into the |listboxDir|
    set(handles.listboxDir,'string',handles.edgesDirichlet);
    
    %%
    % Creates the |dataDir| matrix.
    handles.dataDir=cell(length(handles.edgesDirichlet),2);
    
    %%
    % If there exists a local |mat| file, it loads it to fill in
    % the fields with data taken from the previous next...
    if exist('./dBMT_BC2.mat','file') % reuse the previous data
        load('dBMT_BC2','dataDir');
        handles.dataDir=dataDir;
    %%
    % ... otherwise it just sets all Dirichlet boundary conditions to zero    
    else
        for i=1:length(handles.edgesDirichlet)
            handles.dataDir{i,1}=handles.edgesDirichlet(i);
            handles.dataDir{i,2}='0';
        end
    end
    
    %%
    % Creates the table where the Dirichlet boundary conditions are listed, 
    % along with their types
    column1={'Boundary ID','State field'};
    uitable('units','Normalized','Position',[0.365,0.02,0.11,0.57],...
        'Data',handles.dataDir,'ColumnName',column1,'RowName',[]);
end


%%
% *Neumann boundary conditions*

%%
% Generates the |dataNeu| matrix to store the id of the exterior Neumann 
% edges and their enforced boundary conditions. The |dataNeu| matrix is 
% created from from scratch or imported from the |dBMT_BC2| file, if such 
% file exists (meaning that the previous GUI was left unchanged).

if isfield(handles,'edgesNeumann')==1

    handles.edgesNeumann(handles.edgesNeumann==0)=[];
    
    %%
    % Writes all |edgesNeumann| into the |listboxNeu|
    set(handles.listboxNeu,'string',handles.edgesNeumann);
    
    %%
    % Creates the |dataNeu| matrix.
    handles.dataNeu=cell(length(handles.edgesNeumann),2);
    
    
    %%
    % If there exists a local |mat| file, it loads it to fill in
    % the fields with data taken from the previous next...
    if exist('./dBMT_BC2.mat','file')  % reuse the previous data
        load('dBMT_BC2','dataNeu');
        handles.dataNeu=dataNeu;
    %%
    % ... otherwise it just sets all Neumann boundary conditions to zero
    else
        for i=1:length(handles.edgesNeumann)
            handles.dataNeu{i,1}=handles.edgesNeumann(i);
            handles.dataNeu{i,2}='0';
        end
    end
    
    %%
    % Creates the table where the Neumann boundary conditions are listed, 
    % along with their types
    column2={'Boundary ID','Flux field'};
    uitable('units','Normalized','Position',[0.62,0.02,0.11,0.57],...
        'Data',handles.dataNeu,'ColumnName',column2,'RowName',[]);
end


%%
% *Robin boundary conditions*

%%
% Generates the |dataRobin| matrix to store the id of the exterior Robin 
% edges and their enforced boundary conditions. The |dataRobin| matrix is 
% created from from scratch or imported from the |dBMT_BC2| file, if such 
% file exists (meaning that the previous GUI was left unchanged).

if isfield(handles,'edgesRobin')==1

    handles.edgesRobin(handles.edgesRobin==0)=[];
    
    %%
    % Writes all |edgesRobin| into the |listboxRobin|
    set(handles.listboxRobin,'string',handles.edgesRobin);
    
    %%
    % Creates the |dataRobin| matrix. It stores the edges ids, the Robin
    % field and the Robin constant
    handles.dataRobin=cell(length(handles.edgesRobin),3);
    
    
    %%
    % If there exists a local |mat| file, it loads it to fill in
    % the fields with data taken from the previous next...
    if exist('./dBMT_BC2.mat','file')  % reuse the previous data
        load('dBMT_BC2','dataRobin');
        handles.dataRobin=dataRobin;
    %%
    % ... otherwise it just sets all Robin boundary conditions to zero
    else
        for i=1:length(handles.edgesRobin)
            handles.dataRobin{i,1}=handles.edgesRobin(i);
            handles.dataRobin{i,2}='0';  % Robin field
            handles.dataRobin{i,3}='0';  % Robin constant
        end
    end
    
    %%
    % Creates the table where the Robin boundary conditions are listed, 
    % along with their types
    column2={'Boundary ID','Robin field','Constant'};
    uitable('units','Normalized','Position',[0.754,0.02,0.133,0.28],...
        'Data',handles.dataRobin,'ColumnName',column2,'RowName',[]);
    
    %%
    % Giving instructions on the format of the boundary condition
    mystr = '$$ \frac{\partial u \left( s \right)}{\partial n} = \overline{g} \left( s \right) + r \cdot u \left( s \right) $$';
    try
        annotation(handles.uipanel4,'textbox', [0.47,0.34,0.1,0.1],'string',mystr,...
            'interpreter','latex', 'FontSize',13,'LineStyle','none',...
            'FontWeight','bold');
    catch 
        annotation(gcf,'textbox', [0.02,0.47,0.1,0.1],'string',mystr,...
            'interpreter','latex', 'FontSize',14,'LineStyle','none',...
            'FontWeight','bold');
    end
    
end

%%
% Generates the code for drawing the mesh, along with the mesh information
% buttons

%load the mesh data
load('dBMT_BC1','nodes','edges_nodes');

% getting the geometrical limits of the structure
limxmin = min(nodes(:,1));
limxmax = max(nodes(:,1));
limymin =  min(nodes(:,2));
limymax =  max(nodes(:,2));
nedge = length(edgesArray);
nnode = length(nodes(:,1));

%
% Plotting the Finite Element Mesh
% Initialization of the required matrices
X = zeros(2,nedge) ;
Y = zeros(2,nedge) ;
% Extract X,Y coordinates for the (iel)-th element
for iel = 1:nedge
    X(:,iel) = nodes(edges_nodes(iel,:),1) ;
    Y(:,iel) = nodes(edges_nodes(iel,:),2) ;
end

% drawing the mesh
patch(X,Y,'w'); hold on; plot(X(:),Y(:),'o','MarkerEdgeColor','k',...
    'MarkerFaceColor','k','MarkerSize',15); hold off;
axis([limxmin-0.01*abs(limxmin) limxmax+0.01*abs(limxmax) limymin-0.01*abs(limymin) limymax+0.01*abs(limymax)]);
axis equal;
axis off ;


% To display Node Numbers % Element Numbers
axpos = getpixelposition(handles.axes3); % position & dimension of the axes object
% Define button's weight and height
bweight = 60;
bheight = 20;
pos = [((axpos(1)+axpos(3))/2) (axpos(2)-2.5*bheight) bweight bheight]; % align the second button with the center of the axes obj limit
ShowNodes = uicontrol('style','toggle','string','Nodes',....
    'position',[(pos(1)-2*bweight) pos(2) pos(3) pos(4)],'background',...
    'white','units','Normalized');

ShowEdges = uicontrol('style','toggle','string','Edges',....
    'position',[(pos(1)+1.8*bweight) pos(2) pos(3) pos(4)],'background','white',...
    'units','Normalized');

set(ShowNodes,'callback',...
    {@SHOWNODES,ShowEdges,nodes,edges_nodes,X,Y,nnode,nedge});
set(ShowEdges,'callback',...
    {@SHOWEDGES,ShowNodes,nodes,edges_nodes,X,Y,nnode,nedge});

if ~isfield(handles,'edgesDirichlet')
    set(handles.assignState, 'enable', 'off')
    set(handles.editState, 'enable', 'off')    
    set(handles.listboxDir, 'enable', 'off')    
    set(handles.resetDirichlet, 'enable', 'off')    
end

if ~isfield(handles,'edgesNeumann')
    set(handles.assignFlux, 'enable', 'off')
    set(handles.editFlux, 'enable', 'off')    
    set(handles.listboxNeu, 'enable', 'off')    
    set(handles.resetNeumann, 'enable', 'off')    
end

if ~isfield(handles,'edgesRobin')
    set(handles.assignRobinConstant, 'enable', 'off')
    set(handles.editRobinConstant, 'enable', 'off')    
    set(handles.listboxRobin, 'enable', 'off')    
    set(handles.assignRobin, 'enable', 'off')    
    set(handles.editRobin, 'enable', 'off')    
    set(handles.resetRobin, 'enable', 'off')    
end

%%
% Updates the handles structure
guidata(hObject, handles);



%% NEXT FUNCTION
% * Executes on button press in |next|;
% * Reads the boundary condition data, stores it in the local |mat| file
% and starts the next GUI;
% * If the user asked for the model to be saved, it saves the information
% regarding this GUI in the specified file and folder.

function next_Callback(hObject, eventdata, handles)
% hObject    handle to next (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

%%
% Recovering the user-defined data 
if isfield(handles,'edgesDirichlet')
    edgesDirichlet=handles.edgesDirichlet;
    dataDir=handles.dataDir;
end
if isfield(handles,'edgesNeumann')
    edgesNeumann=handles.edgesNeumann;
    dataNeu=handles.dataNeu;
end
if isfield(handles,'edgesRobin')
    edgesRobin=handles.edgesRobin;
    dataRobin=handles.dataRobin;
    % Checking if null Robin boundary coefficients exist
    if any(cellfun(@str2num,dataRobin(:,3))==0)
        errordlg('Robin boundary constants cannot be zero. If you are sure you want them null, please define a Neumann boundary condition instead.','Invalid input','modal');
        return;
    end
end


%% 
% Saving the local data to save files
% If dBMT_BC2.mat does not exist, it creates it
if ~exist('./dBMT_BC2.mat','file')
    dummy = 0;
    save('dBMT_BC2','dummy');
end

%%
% If requested by the user, appends the local data to the save file.
% Otherwise, it just creates the dBMT_BC2 mat file with the boundary
% condition information.
load('dBMT_StructDef','DirName','FileName');

if isfield(handles,'edgesDirichlet')
    if ~isempty(DirName)
        save(fullfile(DirName,FileName),'-append',...
            'edgesDirichlet','dataDir');
    end
    save('dBMT_BC2','-append','edgesDirichlet','dataDir');
end
if isfield(handles,'edgesNeumann')
    if ~isempty(DirName)
        save(fullfile(DirName,FileName),'-append',...
            'edgesNeumann','dataNeu');
    end
    save('dBMT_BC2','-append','edgesNeumann','dataNeu');
end  
if isfield(handles,'edgesRobin')
    if ~isempty(DirName)
        save(fullfile(DirName,FileName),'-append',...
            'edgesRobin','dataRobin');
    end
    save('dBMT_BC2','-append','edgesRobin','dataRobin');
end

%%
% Closes everything and launches whatever comes next
close(handles.figure1) % closing the GUI window

% Trying to close Visualize if it was opened
try
    close('Visualize');
catch
end

Check;


%% ASSIGN FLUX FUNCTION
% * Executes on button press in |assignFlux|;
% * Fills in the enforced flux table for the boundaries selected in
% |listboxNeu| with the string defined in |editFlux|.

function assignFlux_Callback(hObject, eventdata, handles)
% hObject    handle to assignFlux (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

%%
% Gets the selected boundaries in |listboxNeu| 
itemlist = get(handles.listboxNeu,'Value'); % list of selected items in the listbox
nitems = length(itemlist); % number of selected items in the listbox

%%
% Writes the flux definition in the column of |dataNeu|.
for ii = 1:nitems
    crtitem = itemlist(ii);
    handles.dataNeu{crtitem,2}=get(handles.editFlux,'string');
end

%%
% Updates the handles structure
guidata(hObject,handles);

%%
% Redraws the table where the Neumann boundary conditions are listed
column2={'Boundary ID','Flux field'};
uitable('units','Normalized','Position',[0.62,0.02,0.11,0.57],...
    'Data',handles.dataNeu,'ColumnName',column2,'RowName',[]);


%% ASSIGN STATE FUNCTION
% * Executes on button press in |assignState|;
% * Fills in the enforced state table for the boundaries selected in
% |listboxDir| with the string defined in |editState|.

function assignState_Callback(hObject, eventdata, handles)
% hObject    handle to assignState (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

%%
% Gets the selected boundaries in |listboxDir|
itemlist = get(handles.listboxDir,'Value'); % list of selected items in the listbox
nitems = length(itemlist); % number of selected items in the listbox

%%
% Writes the temperature definition in the column of |dataDir|.
for ii = 1:nitems
    crtitem = itemlist(ii);
    handles.dataDir{crtitem,2}=get(handles.editState,'string');
end

%%
% Updates the handles structure
guidata(hObject,handles);

%%
% Redraws the table where the Dirichlet boundary conditions are listed
column1={'Boundary ID','State field'};
uitable('units','Normalized','Position',[0.365,0.02,0.11,0.57],...
    'Data',handles.dataDir,'ColumnName',column1,'RowName',[]);


%% ASSIGN ROBIN FUNCTION
% * Executes on button press in |assignRobin|;
% * Fills in the enforced Robin field data for the boundaries selected in
% |listboxRobin| with the string defined in |editRobin|.

function assignRobin_Callback(hObject, eventdata, handles)
% hObject    handle to assignState (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

%%
% Gets the selected boundaries in |listboxRobin|
itemlist = get(handles.listboxRobin,'Value'); % list of selected items in the listbox
nitems = length(itemlist); % number of selected items in the listbox

%%
% Writes the temperature definition in the column of |dataRobin|.
for ii = 1:nitems
    crtitem = itemlist(ii);
    handles.dataRobin{crtitem,2}=get(handles.editRobin,'string');
end

%%
% Updates the handles structure
guidata(hObject,handles);

% Redraws the table where the Robin boundary conditions are listed
column2={'Boundary ID','Robin field','Constant'};
uitable('units','Normalized','Position',[0.754,0.02,0.133,0.28],...
    'Data',handles.dataRobin,'ColumnName',column2,'RowName',[]);


%% ASSIGN ROBIN CONSTANT FUNCTION
% * Executes on button press in |assignRobinConstant|;
% * Fills in the Robin constant data for the boundaries selected in
% |listboxRobin| with the string defined in |editRobinConstant|.

function assignRobinConstant_Callback(hObject, eventdata, handles)
% hObject    handle to assignState (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

%%
% Gets the selected boundaries in |listboxRobin|
itemlist = get(handles.listboxRobin,'Value'); % list of selected items in the listbox
nitems = length(itemlist); % number of selected items in the listbox

%%
% Writes the temperature definition in the column of |dataRobin|.
for ii = 1:nitems
    crtitem = itemlist(ii);
    handles.dataRobin{crtitem,3}=get(handles.editRobinConstant,'string');
end

%%
% Updates the handles structure
guidata(hObject,handles);

% Redraws the table where the Robin boundary conditions are listed
column2={'Boundary ID','Robin field','Constant'};
uitable('units','Normalized','Position',[0.754,0.02,0.133,0.28],...
    'Data',handles.dataRobin,'ColumnName',column2,'RowName',[]);




%% RESET NEUMANN/DIRICHLET/ROBIN BOUNDARY CONDITION FUNCTIONS

%%
% *Reset Neumann*
%
% * Executes on button press in |resetNeumann|;
% * Substitutes all previous definitions in |dataNeu| by zero;
% * Redraws the Neumann boundary condition table.

function resetNeumann_Callback(hObject, eventdata, handles)
% hObject    handle to resetNeumann (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

%%
% Substitutes all previous definitions in |dataNeu| by zero
for i=1:length(handles.edgesNeumann)
    handles.dataNeu{i,2} = '0';
end

%%
% Updates the handles structure
guidata(hObject,handles);

%%
% Redraws the table where the Neumann boundary conditions are listed
column2={'Boundary ID','Flux field'};
uitable('units','Normalized','Position',[0.62,0.02,0.11,0.57],...
    'Data',handles.dataNeu,'ColumnName',column2,'RowName',[]);



%%
% *Reset Dirichlet*
%
% * Executes on button press in |resetDirichlet|;
% * Substitutes all previous definitions in |dataDir| by zero;
% * Redraws the Dirichlet boundary condition table.

function resetDirichlet_Callback(hObject, eventdata, handles)
% hObject    handle to resetDirichlet (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

%%
% Substitutes all previous definitions in |dataDir| by zero
for i=1:length(handles.edgesDirichlet)
    handles.dataDir{i,2} = '0';
end

%%
% Updates the handles structure
guidata(hObject,handles);

%%
% Redraws the table where the Dirichlet boundary conditions are listed
column1={'Boundary ID','State field'};
uitable('units','Normalized','Position',[0.365,0.02,0.11,0.57],...
    'Data',handles.dataDir,'ColumnName',column1,'RowName',[]);


%%
% *Reset Robin*
%
% * Executes on button press in |resetRobin|;
% * Substitutes all previous definitions in |dataRobin| by zero;
% * Redraws the Robin boundary condition table.

function resetRobin_Callback(hObject, eventdata, handles)
% hObject    handle to resetRobin (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

%%
% Substitutes all previous definitions in |dataRobin| by zero
for i=1:length(handles.edgesRobin)
    handles.dataRobin{i,2} = '0';
    handles.dataRobin{i,3} = '0';
end

%%
% Updates the handles structure
guidata(hObject,handles);

%%
% Redraws the table where the Dirichlet boundary conditions are listed
column2={'Boundary ID','Robin field','Constant'};
uitable('units','Normalized','Position',[0.754,0.02,0.133,0.28],...
    'Data',handles.dataRobin,'ColumnName',column2,'RowName',[]);



%% PREVIOUS FUNCTION
% * Executes on button press in |previous|;
% * Just closes the current GUI and launches the previous one. All changes
% made in the current GUI are loUI.

function previous_Callback(hObject, eventdata, handles)
% hObject    handle to previous (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
close(dBMT_BC2);

% Trying to close Visualize if it was opened
try
    close('Visualize');
catch
end

dBMT_BC1;


%% ENLARGE FUNCTION
% * Executes on button press in |pushbutton_enlarge|;
% * Just launches the Visualize GUI.

% --- Executes on button press in pushbutton_enlarge.
function pushbutton_enlarge_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton_enlarge (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

Visualize;





%% GUI ENTITIES GENERATION CODE
% * Automatically generated code for the buttons and menus;
% * Some (not-so-sound) checks are performed on the data inserted by the
% user in |editDisp| and |editNeu| just to make sure they are
% mathematically legible. 

% --- Executes on selection change in listboxNeu.
function listboxNeu_Callback(hObject, eventdata, handles)
% hObject    handle to listboxNeu (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns listboxNeu contents as cell array
%        contents{get(hObject,'Value')} returns selected item from listboxNeu


% --- Executes during object creation, after setting all properties.
function listboxNeu_CreateFcn(hObject, eventdata, handles)
% hObject    handle to listboxNeu (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: listbox controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function editFlux_Callback(hObject, eventdata, handles)
% hObject    handle to editFlux (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of editFlux as text
%        str2double(get(hObject,'String')) returns contents of editFlux as a double

[Flux, status] = str2num(get(hObject,'string'));
if any(isnan(Flux)) || ~status  % if the input is something else than
                                     % a vector of reals
    set(hObject,'String','');
    errordlg('All fluxes must have real values','Invalid input','modal');
    uicontrol(hObject);
    return;
end


% --- Executes during object creation, after setting all properties.
function editFlux_CreateFcn(hObject, eventdata, handles)
% hObject    handle to editFlux (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



% --- Executes on selection change in listboxDir.
function listboxDir_Callback(hObject, eventdata, handles)
% hObject    handle to listboxDir (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns listboxDir contents as cell array
%        contents{get(hObject,'Value')} returns selected item from listboxDir


% --- Executes during object creation, after setting all properties.
function listboxDir_CreateFcn(hObject, eventdata, handles)
% hObject    handle to listboxDir (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: listbox controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function editState_Callback(hObject, eventdata, handles)
% hObject    handle to editState (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of editState as text
%        str2double(get(hObject,'String')) returns contents of editState as a double
[T, status] = str2num(get(hObject,'string'));
if any(isnan(T)) || ~status  % if the input is something else than
                                     % a vector of reals
    set(hObject,'String','');
    errordlg('All state values must be real','Invalid input','modal');
    uicontrol(hObject);
    return;
end

% --- Executes during object creation, after setting all properties.
function editState_CreateFcn(hObject, eventdata, handles)
% hObject    handle to editState (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in pushbutton13.
function pushbutton13_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton13 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


function editRobin_Callback(hObject, eventdata, handles)
% hObject    handle to editRobin (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of editRobin as text
%        str2double(get(hObject,'String')) returns contents of editRobin as a double
[R, status] = str2num(get(hObject,'string'));
if any(isnan(R)) || ~status  % if the input is something else than
                                     % a vector of reals
    set(hObject,'String','');
    errordlg('All Robin field values must be real','Invalid input','modal');
    uicontrol(hObject);
    return;
end

% --- Executes during object creation, after setting all properties.
function editRobin_CreateFcn(hObject, eventdata, handles)
% hObject    handle to editRobin (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in listboxRobin.
function listboxRobin_Callback(hObject, eventdata, handles)
% hObject    handle to listboxRobin (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns listboxRobin contents as cell array
%        contents{get(hObject,'Value')} returns selected item from listboxRobin


% --- Executes during object creation, after setting all properties.
function listboxRobin_CreateFcn(hObject, eventdata, handles)
% hObject    handle to listboxRobin (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: listbox controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function editRobinConstant_Callback(hObject, eventdata, handles)
% hObject    handle to editRobinConstant (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of editRobinConstant as text
%        str2double(get(hObject,'String')) returns contents of editRobinConstant as a double
[R, status] = str2num(get(hObject,'string'));
if length(R) > 1 || ~status % if the input is a vector rather 
                                  % than a single number or is not real
    set(hObject,'String','');
    errordlg('A real number is expected','Invalid input','modal');
    uicontrol(hObject);
    return;
end
if isnan(R) > 1  
    set(hObject,'String','');
    errordlg('A real number is expected','Invalid input','modal');
    uicontrol(hObject);
    return;
end

% --- Executes during object creation, after setting all properties.
function editRobinConstant_CreateFcn(hObject, eventdata, handles)
% hObject    handle to editRobinConstant (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



