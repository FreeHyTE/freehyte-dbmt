function Main
% Main is the main routine of FreeHyTE DBM elliptic problems module. 
% FreeHyTE DBM solves homogenous Laplace or Helmholtz equation using 
% three Direct Boundary Methods:
% - the Direct Trefftz-Herrera Method (DTHM);
% - the Direct Method of Fundamental Solution (DMFS); and,
% - the Direct Boundary Element Method (DBEM).
%
% MAIN is called upon exiting the data input (GUI) phase of the module.
% It is used to launch all functions required for the solution of the
% Laplace/Helmholtz problem and centralize all data they provide.
%
% BIBLIOGRAPHY
% 1. FreeHyTE GIT Page 
% 2. Borkowski M, Moldovan ID - Direct Boundary Method Toolbox for some
% elliptic PDEs in FreeHyTE framework, submitted to Advances in Engineering
% Software, 2019

%%
% Setting warnings to off. These warnings are caused by missing files and
% variables before the local |mat| files are written for the first time and
% by the possibility that the problem is purely Neumann or Dirichlet. The
% warnings are re-activated after the successful execution, at the end of
% this function.
warning('off','MATLAB:DELETE:FileNotFound');
warning('off','MATLAB:load:variableNotFound');

%% ******** PRE-PROCESSING ********
% Launch pre-processing routine: InputProc
% Processes the input data from GUI, generates the data structures
% * NGP is the number of Gauss points for the line integration;
% * Nodes is a (NNODE x 2) matrix, where NNODE is the number of nodes in 
% mesh. It stores the coordinates of each node;
% * Edges, Loops and BConds are data structures storing information
% on the edges, domain and boundary conditions, respectively. 
% * offset is the offset of the interpolation nodes of the boundary
% elements from their geometric limits, expressed as a ratio of the length
% of the boundary elements
% * SolMethod designates which solution method should be used (1 - DTHM; 2-
% DMFS; 3 - DBEM)
% * lambda is the distance between real and auxiliary boundaries in the
% dMFS
% EquationType designates the type of problem that should be solved (1 -
% Laplace problem; 2- Helmholtz problem)
% ProblemType designates the type of problem that should be solved (1 -
% interior problem; 2- exterior problem)
% kparam is the wave number (k) parameter in Helmholtz problems. Not used
% in Laplace problems
% incidentWaveAngle is the angle from which incident plane wave is coming
% [NGP, Nodes, Edges, ~, BConds, offset, SolMethod, lambda,...
%     EquationType, ProblemType, kParam, incident_params] = InputProc;

[NGP, Nodes, Edges, ~, BConds, UI] = InputProc;


%% Pre-processing
% ASSIGNPARTS maps the solving system and assigns entries and dimensions to
% the boundary elements. The information is used by the functions that
% generate the blocks of the solving system to insert them at the right
% positions. The entry columns and dimensions are stored in the Edges
% structure (members 'insert' and 'dim').
% * Dim lists the dimensions of the G and H matrices (and of the solving
% system). 
% * the mapping of the solving system is covered in reference [2]
[Edges,Dim] = AssignParts(Edges); 

% Initialization of Gauss-Legendre weights & abscissas (on a -1:1
% interval). gauleg is an external routine, written by Greg von Winckel.
[abscissa, weight] = gauleg(NGP, -1, 1);

% Generating the preprocessing structure which stores collocation nodes,
% domain characteristic length, and optimal place for Trefftz
% eigenexpension (domain barycenter)
[preProcStruc] = preProcessing(Nodes, Edges, UI.offset, UI.MeshOption) ;


%% Processing
if ~(UI.EquationType == 1 ...  % Laplace Equation
     || UI.EquationType == 2 ) % Helmholtz Equation
    error('Ill defined solution method (EquationType = %g). This really shouldn''t have happened!\n',UI.EquationType) ;
end
     
if ~(UI.ProblemType == 1 ...  % interior problem
     || UI.ProblemType == 2 ) % exterior problem
    error('Ill defined type of the problem (ProblemType = %g). This really shouldn''t have happened!\n',UI.ProblemType) ;
end


% Depending on the type of PDE and the solution method chosen by the user
% (in GUI1), matrices G and H are generated. Their expressions and theory
% is covered in reference [2]

if UI.EquationType == 1 % Laplace equation 
    kappa = 0 ;    
elseif UI.EquationType == 2 % Helmholtz equation 
    kappa = UI.k ;
end

% initialize a structure that will be filled with  non-empty fields ...
% only when Helmholtz exterior problem is considered
inc_wave.f = [] ; 
inc_wave.dx =[] ; 
inc_wave.dy =[] ;

if UI.ProblemType == 1 % interior problem
    probType = 'int' ;
    % incident_params = [] ;    
else
    probType = 'ext' ;
    if UI.ProblemType == 2 && UI.EquationType == 2 % exterior Helmholtz problem
        
        % define incident plane wave parameters and function handles                
        A_inc = UI.incident.amplitude ;
        k_inc = UI.k ;
        a_inc = UI.incident.angle ;
        
        % incident plane wave        
        inc_wave.f = @(x,y) A_inc * exp( ...
                        +1i * k_inc * (x *cos(a_inc) + y *sin(a_inc)) ...
                        ) ;

        inc_wave.dx = @(x,y) +1i * k_inc * cos(a_inc) * inc_wave.f(x,y) ;
        inc_wave.dy = @(x,y) +1i * k_inc * sin(a_inc) * inc_wave.f(x,y) ;

    end
end
   
 


switch UI.SolMethod
    case 1 % direct Trefftz(-Herrera) method            
        [G,H,R] = Gen_Matrices_dTHM(Edges, BConds, abscissa, ...
            weight, UI.offset, preProcStruc, kappa, Dim, probType) ;
    case 2 % direct Method of Fundamental Solutions
        [G,H,R] = Gen_Matrices_dMFS(Edges, BConds, abscissa, ...
            weight, UI.offset, preProcStruc, kappa, UI.lambda) ;
    case 3 % direct Boundary Element Method
        [G,H,R] = Gen_Matrices_dBEM(Edges, BConds, abscissa, ...
            weight, UI.offset, preProcStruc, kappa) ; 
    otherwise
        error('Ill defined solution method (SolMethod = %g). This really shouldn''t have happened!\n',SolMethod);
end


%%% modification of BCs in case of incident wave problems
if ~isempty(inc_wave.f) && UI.incident.amplitude ~= 0
    
    [BConds] = BCondsForIncidentWaveProblem(Edges,BConds,UI.offset, inc_wave) ;
            
end  



% The solving system of any direct method is obtained by separating the
% expressions involving known and unknown nodal values, according to the
% boundary conditions specified by the user.
[A,b, known] = Gen_Sys(Edges,BConds,UI.offset, H, G, R) ; 

%% Scaling of the system
% the scaling procedure is covered in reference [2]
if UI.SolMethod ~= 3    % scaling is not implemented for the dBEM
    % the scaling factors are the inverse of the sqare root of the maximum
    % terms on each line and column of the system
    ScL = diag((max(abs(A),[],2)).^-0.5);
    ScC = diag((max(abs(A))).^-0.5);
    
    % If the scaling term is null, the respective line or column remain
    % unscaled.
    ScL(isinf(ScL)) = 1;
    ScC(isinf(ScC)) = 1;
    
    % Scaling the A matrix. Old A is overwritten by its scaled version
    % in order to save memory
    A = ScL * A * ScC;
    % Scaling the b vector. Old b is overwritten by its scaled version
    % in order to save memory
    b = ScL * b;
end

%% Solution of the solving system A*x = b
switch UI.SolMethod
    case 1 % direct Trefftz(-Herrera) method
        [mm,nn] = size(A) ;
        if mm~=nn || rcond(A) < eps
            x = pinv(A) * b ;
        else
            x = A\b ;
        end
    case 2 % direct Method of Fundamental Solutions
        if rcond(A) < eps
            x = pinv(A) * b ;
        else
            x = A\b ;
        end
    case 3 % direct Boundary Element Method
        
        x = A\b ; 
        
    otherwise
        error('Ill defined solution method (SolMethod = %g). This really shouldn''t have happened!\n',SolMethod);
end

%% Scaling back x
if UI.SolMethod ~= 3
    x = ScC * x;
end

%% Post-processing
% rearranging the solution vector in order to obtain vectors of u and q 
% node values
if any(Edges.type == 'R')
    % if there exist Robin BC additional computations need to be conducted
    [u, q] = AssembleSolutionVector(x, known, Edges, BConds, UI.offset) ;
else
    [u, q] = AssembleSolutionVector(x, known, Edges) ;
end

% % % Plotting the potential and flux fields
%% Post-processing using layer potentials 
% localPointsForPlotting designate the points where the solution should be
% calculated inside the domain. For this purpose, the domain is meshed
% into subdomains using the standard finite element generators built into
% FreeHyTE (see the CreateRegMesh and CreateEdgeLoop functions) and a mesh
% of length(localPointsForPlotting)^2 of plotting points is created in each
% subdomain.
% The definition of the plotting points in each subdomain should be handled
% with care. Plotting points that are too close to the boundary may cause
% spurious oscillations of the solutions because of the proximity to the
% origin of the Green's functions (see reference [2]).
localPointsForPlotting = [0] ; % [-0.8 -0.4  0  0.4 0.8] %[0] ; %[-0.8 0.8] ;
PointsCoord = GenPlotPoints(localPointsForPlotting) ;

X = PointsCoord(:,1) ; 
Y = PointsCoord(:,2) ; 

% including corner nodes in the plot of the final solution when dealing
% with exterior problem
if UI.ProblemType == 2 
    load('dBMT_StructDef','p');
    load('irreg_mesh', 'e', 'ePolygon','polygonNo') ;

    % Edges of external polygon ;
    EDGES_OF_EXT_POLYGON = e(:, (ePolygon(1,:) == 1) ) ; 
    % Nodes of external polygon
    NODES_OF_EXT_POLYGON = EDGES_OF_EXT_POLYGON(1,:) ;
    Px = p(1, NODES_OF_EXT_POLYGON) ;
    Py = p(2, NODES_OF_EXT_POLYGON) ;
    % include nodes of external polygon into the set generated by GenPlotPoints()
    X = [X; Px(:)] ;
    Y = [Y; Py(:)] ;
end

[U, dU_dx, dU_dy, uBound, qxBound, qyBound, xBound, yBound] = ...
            ComputeFieldsFromLayerPotentials(Edges, u,q, X, Y, ...
                    abscissa, weight, UI.offset, kappa) ;          
                
% add incident wave to the solution if needed
if ~isempty(inc_wave.f) && UI.incident.amplitude ~= 0
    U = U + inc_wave.f(X,Y) ;
    dU_dx = dU_dx + inc_wave.dx(X,Y) ;
    dU_dy = dU_dy + inc_wave.dy(X,Y) ;
    
    uBound = uBound + inc_wave.f(xBound,yBound) ;
    qxBound = qxBound + inc_wave.dx(xBound,yBound) ;
    qyBound = qyBound + inc_wave.dy(xBound,yBound) ;
end 
                
PlotFieldsOriginatedByLayerPotentials([X(:); xBound(:)] , ...
        [Y(:); yBound(:)],[U(:); uBound(:)], [dU_dx(:); qxBound(:)], ...
                [dU_dy(:); qyBound(:)]) ; 


figure, surfScatteredData([X(:); xBound(:)] ,[Y(:); yBound(:)], real( [U(:); uBound(:) ] ) ) ;
%%
% colorbar
% colormap(jet)
% view([0 0 180])
% export_fig
% saveas(gcf, 'THM', 'epsc')
% title('Numerical 3D solution')
% save('Examples/aux_solutionOfScatteringProblem', 'X','Y','U','xBound', 'yBound', 'uBound')

%%
% hold on 
% plot3(preProcStruc.TreffC(1), preProcStruc.TreffC(2), 1.001 * max( real( [U(:); uBound(:) ] ) ), 'r*')
% hold off




%%
% Re-enabling warnings.
warning('off','MATLAB:DELETE:FileNotFound');
warning('off','MATLAB:load:variableNotFound');

end

