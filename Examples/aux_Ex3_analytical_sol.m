% Auxiliary script with analytical solution of scattering by cylindrical
% obstacle problem

clear all

% derivatives of bessel and hankel functions
d_besselj = @(nu,k,r) k/2 * (besselj(nu-1, k*r) - besselj(nu+1, k*r) ) ;
d_besselh = @(nu,k,r) k/2 * (besselh(nu-1, k*r) - besselh(nu+1, k*r) ) ;

% Parameters of incident wave and problem geometry
k = 2 ;  % wavenumber
A = 1 ;  % amplitude of incident wave
a = 0.4; % radius of cylindrical obstacle with a centre in (0,0)

% parameters for analytical solution
N = 21 ; % number of terms in series
e_n = 2*ones(1,N) ; e_n(1) = 1 ; 

%%%%%%%%%%%%%%%%%%%%%%%%%
% mesh for 3D plot

x = -2:0.1:2 ;
y = -2:0.1:2 ;
[X,Y] = meshgrid(x,y) ;
[T,R] = cart2pol(X,Y) ;
% total solution
U_T = zeros(size(T)) ;

for nn = 1:N 
    n = nn-1 ;   
    
    U_T = U_T + A * e_n(nn) * i^n * (...
                  besselj(n,k*R)  - ...
                  d_besselj(n,k,a)/d_besselh(n,k,a) .* besselh(n,k*R) ) ...
                .* cos(n*T) ;
end

%%%%%%%%%%%%%%%%%%%%%%%%%
% characteristic plot
aa = a ;
tt = linspace(0,2*pi, 361) ;
rr = aa * ones(size(tt)) ;
% total solution
ut = zeros(size(tt)) ;

for nn = 1:N      
    n = nn-1 ;
    
    ut = ut + A * e_n(nn) * i^n * ( ...
                besselj(n,k*rr)  - ...
                d_besselj(n,k,aa)/d_besselh(n,k,aa) .* besselh(n,k*rr) )...
              .* cos(n*tt) ;
end


%%%%%%%%%%%%%%%%%%%%%%%%%

U_T( sqrt(X.^2+Y.^2)<0.1 ) = NaN ;
figure, surfc(X,Y, real(U_T)); 

title('Analytical solution')
shading interp

figure, polar(tt, abs(ut)); title('Characteristic')



% %%%%%%%%%%%%%%%%%%%%%%%%%
% %% Computations for Example 3 in the paper
% 
% load('solutionOfScatteringProblem', 'X','Y','U','xBound', 'yBound', 'uBound')
% 
% [T,R] = cart2pol(X,Y) ;
% [tBound,rBound] = cart2pol(xBound,yBound) ;
% 
% U_T = zeros(size(T)) ;
% u_TBound = zeros(size(tBound)) ;
% 
% for nn = 1:N      
%     n = nn-1 ;   
%     
%     
%     a_n = A * e_n(nn) * i^n * (...
%                   besselj(n,k*R)  - ...
%                   d_besselj(n,k,a)/d_besselh(n,k,a) .* besselh(n,k*R) ) ...
%                 .* cos(n*T) ;
%     
%     U_T = U_T + a_n ;
%             
%     u_TBound = u_TBound + A * e_n(nn) * i^n * (...
%                   besselj(n,k*rBound)  - ...
%                   d_besselj(n,k,a)/d_besselh(n,k,a) .* besselh(n,k*rBound) ) ...
%                 .* cos(n*tBound) ;
% 
%     if nn == 1 
%         a_1_max = abs(max(max(a_n))) ;
%     else
%         if ( abs(max(max(a_n))) / a_1_max )  < 10^-4
%             % Stop the loop if the term is small enough
%             break;
%         end
%     end   
%             
% end
% 
% %% Error computation
% errDomainAbs = abs(real(U)-real(U_T)) ;
% errDomainRel = (errDomainAbs./real(max(U_T))) ;
% 
% maxRelDomErr = max(errDomainRel)
% 
% errBoundAbs = abs(real(uBound)-real(u_TBound)) ;
% errBoundRel = (errBoundAbs./real(max(u_TBound))) ;
% maxRelBoundErr = max(errBoundRel)
% 
% 
% figure, surfScatteredData([X(:); xBound(:)] ,[Y(:); yBound(:)], real( [U_T(:); u_TBound(:) ] ) ) ;
% 
% % colorbar
% % colormap(jet)
% % view([0 0 180])
% % export_fig
% % saveas(gcf, 'Exact', 'epsc')
% 
% title('Exact')
% figure, surfScatteredData([X(:); xBound(:)] ,[Y(:); yBound(:)], real( [U(:); uBound(:) ] ) ) ;
% title('Numerical')
% 
% figure, surfScatteredData([X(:); xBound(:)] ,[Y(:); yBound(:)], real( [errDomainAbs(:); errBoundAbs(:) ] ) ) ; 
% title('|error|')
% figure, surfScatteredData([X(:); xBound(:)] ,[Y(:); yBound(:)], real( [errDomainRel(:); errBoundRel(:) ] ) ) ; 
% title('Relative error')




