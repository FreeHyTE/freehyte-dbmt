function [BConds] = BCondsForIncidentWaveProblem(Edges,BConds,offset, inc_wave)

% BCONDSFORINCIDENTWAVEPROBLEM modifies boundary conditions imposed by the 
% to user by taking into account the presence of incident field (plane wave).
                           
% Total solution of the problem is assumed as:
% u_t = u_i + u_s
% u_i is known. We need to calculate u_s. In order to do that we
% need to recalculate the imposed boundary conditions 
% for u_s part of the solution:
% Dir: u_s = u_t - u_i
% Neu: q_s = q_t - q_i      % q_i needs to be calculated from u_i
% Rob: q_s = r*u_s + g_ext where:
        %            g_ext = q + r*u_i - q_i  % q_i needs to be calculated from u_i

        
% The crucial parts of the function can be moved to Gen_Sys function to
% avoid running loop twice over Edges.
        
% Input data: 
% Edges and BConds -- structures that describe the problem
% offset           -- the offset of the interpolation nodes of the boundary
%                       elements from their geometric limits
% inc_wave         -- function handles for plane wave and its spatial
%                       derivatives

% Output data:
% BConds - modified boundary conditions


for ii=1:length(Edges.nini)
    
    LocEdge =  struct('id',ii,'type',Edges.type(ii), ...
        'parametric',Edges.parametric(ii,:), ...
        'order',Edges.order(ii),'insert',Edges.insert(ii),...
        'dim',Edges.dim(ii));
    
    if strcmp(LocEdge.type,'D')
        % if Dirichlet boundary condition on the element:
                     
        % obtaining the equally spaced points on [-1,1] interval to make
        % them interpolation nodes of incident wave
        a = (length(BConds.Dirichlet{LocEdge.id})~=1) * ...
            linspace(-1,1,length(BConds.Dirichlet{LocEdge.id})) ;
        
        x_equally_spaced = LocEdge.parametric(1)  + 0.5 * (a + 1) * LocEdge.parametric(3) ;
        y_equally_spaced = LocEdge.parametric(2)  + 0.5 * (a + 1) * LocEdge.parametric(4) ;
        
        inc_wave_in_a_points = inc_wave.f(x_equally_spaced, y_equally_spaced) ;
        
        BConds.Dirichlet{ii} = BConds.Dirichlet{ii} - inc_wave_in_a_points ;
        
    end % if strcmp(Edges.type(ii),'D')
    
    if strcmp(LocEdge.type,'N')
        % if Neumann boundary condition on the element:

        % obtaining the equally spaced points on [-1,1] interval to make
        % them interpolation nodes of incident wave
        a = (length(BConds.Dirichlet{LocEdge.id})~=1) * ...
            linspace(-1,1,length(BConds.Dirichlet{LocEdge.id})) ;

        x_equally_spaced = LocEdge.parametric(1)  + 0.5 * (a + 1) * LocEdge.parametric(3) ;
        y_equally_spaced = LocEdge.parametric(2)  + 0.5 * (a + 1) * LocEdge.parametric(4) ;
        
        L = sqrt((LocEdge.parametric(3))^2 + (LocEdge.parametric(4))^2);   
        nx = LocEdge.parametric(4) / L;
        ny = -1* LocEdge.parametric(3) / L;
        
%         inc_wave_DX_in_a_points = inc_wave.dx(x_equally_spaced, y_equally_spaced) ;
%         inc_wave_DY_in_a_points = inc_wave.dY(x_equally_spaced, y_equally_spaced) ;

        du_inc_dn = (inc_wave.dx(x_equally_spaced, y_equally_spaced) * nx + ...
                     inc_wave.dy(x_equally_spaced, y_equally_spaced) * ny)  ;
                 
        BConds.Neumann{ii} = BConds.Neumann{ii} - du_inc_dn ;
                 
    end % if strcmp(Edges.type(ii),'N')
    
    
    if strcmp(LocEdge.type,'R')             
        % if Robin boundary condition on the element:

        % BConds.Robin{ii,2}  -> r
        % BConds.Robin{ii,1}  -> g(s)

        % du_dn = r u + g   equiv. to
           %    du_dn =  BConds.Robin{ii,2} u +  BConds.Robin{ii,1}

        % for external problem - for scattered wave  (us - scattered)
                % dus_dn = r us + g_ext, where g_ext = g + ru - q
           
           
        % obtaining the equally spaced points on [-1,1] interval to make
        % them interpolation nodes of incident wave
        a = (length(BConds.Dirichlet{LocEdge.id})~=1) * ...
            linspace(-1,1,length(BConds.Dirichlet{LocEdge.id})) ;

        x_equally_spaced = LocEdge.parametric(1)  + 0.5 * (a + 1) * LocEdge.parametric(3) ;
        y_equally_spaced = LocEdge.parametric(2)  + 0.5 * (a + 1) * LocEdge.parametric(4) ;
        
        L = sqrt((LocEdge.parametric(3))^2 + (LocEdge.parametric(4))^2);   
        nx = LocEdge.parametric(4) / L;
        ny = -1* LocEdge.parametric(3) / L;
        
%         inc_wave_DX_in_a_points = inc_wave.dx(x_equally_spaced, y_equally_spaced) ;
%         inc_wave_DY_in_a_points = inc_wave.dY(x_equally_spaced, y_equally_spaced) ;

        inc_wave_in_a_points = inc_wave.f(x_equally_spaced, y_equally_spaced) ;
        du_inc_dn = (inc_wave.dx(x_equally_spaced, y_equally_spaced) * nx + ...
                     inc_wave.dy(x_equally_spaced, y_equally_spaced) * ny)  ;

        % r remains the same        
                % BConds.Robin{ii,1} = BConds.Robin{ii,1} ;   
        % g(s) needs modification -> g_ext
        BConds.Robin{ii,1} = ...
                      BConds.Robin{ii,1} ...                        %   g 
                    + BConds.Robin{ii,2} * inc_wave_in_a_points ... % +ru                    
                    - du_inc_dn ;                                   % - q
        
    end % if strcmp(Edges.type(ii),'R')
                
end % for ii=1:length(Edges.nini)



